#include "Geometry.h"
#include <GL\glew.h>
#include <iostream>
#include <cfloat>

#define BUFFER_OFFSET(offset) ((void*)(offset))

Geometry::Geometry(GLenum drawmode, Patch patch, const BoundingBox& box) {
	_drawmode = drawmode;
	_patch = patch;
	_vsize = patch.vsize;
	_csize = patch.csize;
	_data = boost::shared_ptr<GLfloat>(patch.patchVertices);
	_numVerts = patch.numTotalVertices;
	_primcount = patch.numPatches;
	if (_primcount > 1) {
		_startOffsets = new int[_primcount];
		_counts = new int[_primcount];
		for (int i = 0; i < _primcount; i++) {
			_startOffsets[i] = i * _patch.numPatchVertices;
			_counts[i] = _patch.numPatchVertices;
		}
	}
	init();
	if (!box.isValid()) {
		initBoundingBox();
	}
	else {
		_box = box;
	}
}

Geometry::Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, const BoundingBox& box) {
	_drawmode = drawmode;
	_data = data;
	_numVerts = numVerts;
	_vsize = vsize;
	_csize = csize;
	init();
	if (!box.isValid()) {
		initBoundingBox();
	}
	else {
		_box = box;
	}
}

Geometry::Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, GLint* startOffsets, GLsizei* counts, int primcount, const BoundingBox& box) {
	_drawmode = drawmode;
	_data = data;
	_numVerts = numVerts;
	_vsize = vsize;
	_csize = csize;
	_startOffsets = startOffsets;
	_counts = counts;
	_primcount = primcount;
	init();
	if (!box.isValid()) {
		initBoundingBox();
	}
	else {
		_box = box;
	}
}

Geometry::Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, const BoundingBox& box) {
	_drawmode = drawmode;
	_data = data;
	_indices = indices;
	_numVerts = numVerts;
	_numIndices = numIndices;
	_vsize = vsize;
	_csize = csize;
	_counts = new int[1];
	_counts[0] = vsize;
	_useElements = true;
	init();
	if (!box.isValid()) {
		initBoundingBox();
	}
	else {
		_box = box;
	}
}

Geometry::Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, int* counts, int primcount, const BoundingBox& box) {
	_drawmode = drawmode;
	_data = data;
	_indices = indices;
	_numVerts = numVerts;
	_numIndices = numIndices;
	_vsize = vsize;
	_csize = csize;
	_counts = counts;
	_primcount = primcount;
	_useElements = true;
	GLuint** indicesOffset = new GLuint*[primcount];
	for (int i = 0; i < primcount; i++) {
		indicesOffset[i] = new GLuint;
		if (i == 0) {
			indicesOffset[i] = 0;
			continue;
		}
		indicesOffset[i][0] = counts[i - 1];
	}
	_indicesOffset = indicesOffset;
	init();
	if (!box.isValid()) {
		initBoundingBox();
	}
	else {
		_box = box;
	}
}

Geometry::Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, boost::shared_ptr<glm::mat4> mats, int numMats, const BoundingBox& box) {
	_drawmode = drawmode;
	_data = data;
	_indices = indices;
	_numVerts = numVerts;
	_numIndices = numIndices;
	_vsize = vsize;
	_csize = csize;
	_mats = mats;
	_numMats = numMats;
	_useElements = true;
	init();
	//if (!box.isValid()) {
		initBoundingBox();
	//}
	//else {
	//	_box = box;
	//}
}

void Geometry::initBoundingBox() {
	glm::vec4 min = glm::vec4(FLT_MAX, FLT_MAX, FLT_MAX, 1.0f);
	glm::vec4 max = glm::vec4(-FLT_MAX, -FLT_MAX, -FLT_MAX, 1.0f);
	GLfloat* data = _data.get();
	for (int i = 0; i < _numVerts; i++) {
		int idx = i * (_vsize + _csize);
		min.x = data[idx] < min.x ? data[idx] : min.x;
		min.y = data[idx + 1] < min.y ? data[idx + 1] : min.y;
		min.z = data[idx + 2] < min.z ? data[idx + 2] : min.z;

		max.x = data[idx] > max.x ? data[idx] : max.x;
		max.y = data[idx+1] > max.y ? data[idx+1] : max.y;
		max.z = data[idx+2] > max.z ? data[idx+2] : max.z;
	}

	//instancing constructor is called.
	if (_mats != NULL) {
		glm::vec4 mintrans = glm::vec4(FLT_MAX, FLT_MAX, FLT_MAX, 1.0f);
		glm::vec4 maxtrans = glm::vec4(-FLT_MAX, -FLT_MAX, -FLT_MAX, 1.0f);

		for (int i = 0; i < _numMats; i++) {
			glm::mat4 mat = _mats.get()[i];
			glm::vec4 minpt = mat * min;
			glm::vec4 maxpt = mat * max;

			mintrans.x = minpt.x > mintrans.x ? mintrans.x : minpt.x;
			mintrans.y = minpt.y > mintrans.y ? mintrans.y : minpt.y;
			mintrans.z = minpt.z > mintrans.z ? mintrans.z : minpt.z;

			maxtrans.x = maxpt.x < maxtrans.x ? maxtrans.x : maxpt.x;
			maxtrans.y = maxpt.y < maxtrans.y ? maxtrans.y : maxpt.y;
			maxtrans.z = maxpt.z < maxtrans.z ? maxtrans.z : maxpt.z;
		}

		_box = BoundingBox(mintrans, maxtrans);
		return;
	}
	_box = BoundingBox(min, max);
}

void Geometry::setWireframeMode(bool wireframe) {
	_wireframe = wireframe;
}

bool Geometry::getWireframeMode() const {
	return _wireframe;
}

bool Geometry::init() {
	//generate all vbos.
	glGenBuffers(1, &_posBufferId);
	if (_posBufferId <= 0) {
		fprintf(stderr, "Error: failed creating vbo for vertex positions");
		return false;
	}
	GLfloat* data = _data.get();
	glBindBuffer(GL_ARRAY_BUFFER, _posBufferId);
	int buffersize = (_numVerts * _vsize + _numVerts * _csize) * sizeof(float);
	glBufferData(GL_ARRAY_BUFFER, buffersize, data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	if (_useElements) {
		glGenBuffers(1, &_indicesBufferId);
		if (_indicesBufferId <= 0) {
			fprintf(stderr, "Error: failed creating vbo for	elements");
			return false;
		}
		GLuint* indices = _indices.get();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indicesBufferId);
		int indicesSize = _numIndices * sizeof(GLuint);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, indices, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	//setup needed for instancing
	if (_mats != NULL) {
		glm::mat4* mats = _mats.get();
		glGenBuffers(1, &_matInstanceId);
		if (_matInstanceId <= 0) {
			fprintf(stderr, "Error: failed creating vbo for matrix instances");
			return false;
		}
		glBindBuffer(GL_ARRAY_BUFFER, _matInstanceId);
		glBufferData(GL_ARRAY_BUFFER, _numMats * sizeof(glm::mat4), mats, GL_STATIC_DRAW);
		std::cout << "num mats: " << _numMats << " sizeof(mat4): " << sizeof(glm::mat4) << std::endl;
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	//generate vaos and store the buffers.
	glGenVertexArrays(1, &_vaoId);
	if (_vaoId <= 0) {
		return false;
	}
	glBindVertexArray(_vaoId);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, _posBufferId);
	int attribStride = (_vsize + _csize)*sizeof(GLfloat);
	glVertexAttribPointer(0, _vsize, GL_FLOAT, false, attribStride, BUFFER_OFFSET(0));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, _csize, GL_FLOAT, false, attribStride, BUFFER_OFFSET((_vsize)*sizeof(GLfloat)));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	if (_mats != NULL) {
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glEnableVertexAttribArray(5);

		glBindBuffer(GL_ARRAY_BUFFER, _matInstanceId);

		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(float) * 0));
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(float) * 4));
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(float) * 8));
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(float) * 12));
		glVertexAttribDivisor(2, 1);
		glVertexAttribDivisor(3, 1);
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		
	}
	glBindVertexArray(0);

	if (_drawmode == GL_PATCHES) {
		GLint maxPatchVertices = 0;
		glGetIntegerv(GL_MAX_PATCH_VERTICES, &maxPatchVertices);
		std::cout << "Max patch vertices supported: " << maxPatchVertices << std::endl;
	  
		glPatchParameteri(GL_PATCH_VERTICES, _patch.numPatchVertices);
		if (_patch.tcsAvailable == false) {
			glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, _patch.outerLevel);
			glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, _patch.innerLevel);
		}
	}

	return true;
}

void Geometry::setNormals(boost::shared_ptr<GLfloat> normals, int numNormals) {
	_normals = normals;
	_numNormals = numNormals;
	initNormals();
}

bool Geometry::initNormals() {
	glGenBuffers(1, &_normalBufferId);
	if (_normalBufferId <= 0) {
		fprintf(stderr, "Error: failed creating vbo for normals");
		return false;
	}
	GLfloat* normals = _normals.get();
	glBindBuffer(GL_ARRAY_BUFFER, _normalBufferId);
	int buffersize = _numNormals * 3 * sizeof(float);
	glBufferData(GL_ARRAY_BUFFER, buffersize, normals, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(_vaoId);

	glEnableVertexAttribArray(6);
	glBindBuffer(GL_ARRAY_BUFFER, _normalBufferId);
	glVertexAttribPointer(6, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(0));
	
	glBindVertexArray(0);
}

void Geometry::draw() const {
	if (_wireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); 
	}
	else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (_drawmode == GL_PATCHES && _patch.tcsAvailable == false) {
		glPatchParameterfv(GL_PATCH_DEFAULT_OUTER_LEVEL, _patch.outerLevel);
		glPatchParameterfv(GL_PATCH_DEFAULT_INNER_LEVEL, _patch.innerLevel);
	}
	if (_drawmode == GL_POINTS) {
		glPointSize(15.0f);
	}
	glBindVertexArray(_vaoId);
	if (_useElements) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _indicesBufferId);
		if (_primcount > 1) {
			glMultiDrawElements(_drawmode, _counts, GL_UNSIGNED_INT, (GLvoid**)_indicesOffset, _primcount);
			//for (int i = 0; i < _primcount; i++) {
			//	glDrawElements(drawstyle, _counts[i], GL_UNSIGNED_INT, _indicesOffset[i]);
			//}
			//glDrawElements(drawstyle, _counts[0], GL_UNSIGNED_INT, _indicesOffset[0]);
		}
		else {
			if (_mats != NULL)	 {
				glDrawElementsInstanced(_drawmode, _numIndices, GL_UNSIGNED_INT, 0, _numMats);
			}
			else {
				glDrawElements(_drawmode, _numIndices, GL_UNSIGNED_INT, 0);
			}
		}
		
	}
	else {
		if (_primcount > 1) {
			glMultiDrawArrays(_drawmode, _startOffsets, _counts, _primcount);
		}
		else {
			glDrawArrays(_drawmode, 0, _numVerts);
		}
	}
	glBindVertexArray(0);

}

BoundingBox Geometry::getBoundingBox() const {
	return _box;
}

void Geometry::dispose() {
	glDeleteBuffers(1, &_posBufferId);
	glDeleteVertexArrays(1, &_vaoId);
}

void Geometry::setPatchOuterLevel(const glm::vec4& outerlevel) {
	_patch.outerLevel[0] = outerlevel[0];
	_patch.outerLevel[1] = outerlevel[1];
	_patch.outerLevel[2] = outerlevel[2];
	_patch.outerLevel[3] = outerlevel[3];
}

void Geometry::setPatchInnerLevel(const glm::vec2& innerlevel) {
	_patch.innerLevel[0] = innerlevel[0];
	_patch.innerLevel[1] = innerlevel[1];
}

glm::vec4 Geometry::getPatchOuterLevel() const {
	return glm::vec4(_patch.outerLevel[0], _patch.outerLevel[1], _patch.outerLevel[2], _patch.outerLevel[3]);
}

glm::vec2 Geometry::getPatchInnerLevel() const {
	return glm::vec2(_patch.innerLevel[0], _patch.innerLevel[1]);
}

Geometry::~Geometry() {
	std::cout << "destroying geometry" << std::endl;
	dispose();
}
