#version 400 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 tesPosition[3];
in vec4 tesPatchDistance[3];

out vec3 gFaceNormal;
out vec4 gPatchDistance;
out vec3 gTriDistance;

in vec3 tes_color[3];
out vec3 gv_color;

uniform mat3 normalMat;
uniform int gEnableLight = 0;

void main() {
	if(gEnableLight == 1) {
		vec3 A = tesPosition[2] - tesPosition[0];
		vec3 B = tesPosition[1] - tesPosition[0];
		gFaceNormal = normalMat * normalize(cross(A, B));
	}

	gPatchDistance = tesPatchDistance[0];
	gTriDistance = vec3(1, 0, 0);
	gl_Position = gl_in[0].gl_Position;
	gv_color = tes_color[0];
	EmitVertex();

	gPatchDistance = tesPatchDistance[1];
	gTriDistance = vec3(0, 1, 0);
	gl_Position = gl_in[1].gl_Position;
	gv_color = tes_color[1];
	EmitVertex();

	gPatchDistance = tesPatchDistance[2];
	gTriDistance = vec3(0, 0, 1);
	gl_Position = gl_in[2].gl_Position;
	gv_color = tes_color[2];
	EmitVertex();

	EndPrimitive();
}
