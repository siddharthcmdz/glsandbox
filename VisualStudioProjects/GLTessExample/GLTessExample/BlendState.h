#pragma once
#include "State.h"

class BlendState : public State {
private:
	GLenum _srfFactor = GL_SRC_ALPHA;
	GLenum _dstFactor = GL_ONE_MINUS_SRC_ALPHA;
	GLenum _mode = GL_FUNC_ADD;
public:
	BlendState();
	BlendState(GLenum srcFactor, GLenum dstFactor);
	void setBlendEquation(GLenum mode);
	bool apply() const ;
	bool unapply() const ;
	~BlendState();
};

