#include "TransformableGeometry.h"

TransformableGeometry::TransformableGeometry(GLenum drawmode, Patch patch, const BoundingBox& box) : Geometry(drawmode, patch, box) {
	initMat();
}

TransformableGeometry::TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, const BoundingBox& box) : Geometry(drawmode, data, numVerts, vsize, csize, box){
	initMat();
}

TransformableGeometry::TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, GLint* startOffsets, GLsizei* counts, int primcount, const BoundingBox& box) :
Geometry(drawmode, data, numVerts, vsize, csize, startOffsets, counts, primcount, box)
{
	initMat();
}

TransformableGeometry::TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, const BoundingBox& box) :
Geometry(drawmode, data, indices, numVerts, numIndices, vsize, csize, box){
	initMat();
}

TransformableGeometry::TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, int* counts, int primcount, const BoundingBox& box) :
Geometry(drawmode, data, indices, numVerts, numIndices, vsize, csize, counts, primcount, box){
	initMat();
}

TransformableGeometry::TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, boost::shared_ptr<glm::mat4> mats, int numMats, const BoundingBox& box) :
Geometry(drawmode, data, indices, numVerts, numIndices, vsize, csize, mats, numMats, box) {
	initMat();
}

void TransformableGeometry::draw() const {
	Geometry::draw();
}

void TransformableGeometry::initMat() {
	_model = glm::mat4(1.0f);
}

void TransformableGeometry::setModelMatrix(glm::mat4& modelMat) {
	_model = modelMat;
}

glm::mat4 TransformableGeometry::getModelMatrix() const {
	return _model;
}

TransformableGeometry::~TransformableGeometry() {
}
