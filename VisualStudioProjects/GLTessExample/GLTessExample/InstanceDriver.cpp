//#define FREEGLUT_LIB_PRAGMAS 0
//#include <GL\glew.h>
//#include <GL\freeglut.h>
//#include <iostream>
//#include "Geometry.h"
//#include "ShaderSource.h"
//#include "ShaderUtil.h"
//#include "ShaderProgram.h"
//#include "TransformableGeometry.h"
//#include "Scene.h"
//#include <algorithm>
//#include <vector>
//#include <gtc\matrix_transform.hpp>
//#include <gtc\random.hpp>
//#include "TubeData.h"
//#include "GLUtils.h"
//#include "ShaderParameters.h"
//#include "ObjFileReader.h"
//#include <boost\shared_ptr.hpp>
//#include "UniformBufferObject.h"
//#include <ctime>
//
//using namespace std;
//
//#define BUFFER_OFFSET(offset) (static_cast<GLuint*>(0) + (offset))
//
//Scene* g_scene;
//int g_lastX = -1, g_lastY = -1;
//bool g_dragging = false;
//bool g_button1Pressed = false;
//bool g_button2Pressed = false;
//bool g_button3Pressed = false;
//int g_canvasDim[2] = { 500, 500 };
//float g_currViewAngle[2] = { 0.0f, 0.0f };
//float g_viewAngle[2] = { 0.0f, 0.0f };
//float g_transZ = 0.0f;
//float g_currtransZ = 0.0;
//float g_pan[2] = { 0.0f, 0.0f };
//float g_currPan[2] = { 0.0f, 0.0f };
//bool g_enableManipulateLight = false;
//bool g_enableWireframe = false;
//boost::shared_ptr<std::vector<UniformBufferObject>> g_uboParams;
//
//boost::shared_ptr<ShaderParameters> g_meshParams;
//
//boost::shared_ptr<glm::mat4> getMatrices(float rad, int r, int c) {
//	glm::mat4* mats = new glm::mat4[r*c];
//
//	for (int i = 0; i < r; i++) {
//		for (int j = 0; j < c; j++) {
//			glm::mat4 trans = glm::translate(glm::mat4(1.0f), glm::vec3(j*rad, 0.0f, i*rad));
//			int idx = i*c + j;
//			mats[idx] = trans;
//		}
//	}
//	
//	boost::shared_ptr<glm::mat4> matrices = boost::shared_ptr<glm::mat4>(mats);
//
//	return matrices;
//}
//
//static void initObjScene() {
//	ShaderSource* vsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\instancePerFragLit.vert", GL_VERTEX_SHADER);
//	ShaderSource* gsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\instancePerFragLit.gs", GL_GEOMETRY_SHADER);
//	ShaderSource* fsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\instancePerFragLit.frag", GL_FRAGMENT_SHADER);
//	std::vector<ShaderSource> sources;
//	sources.push_back(*vsource);
//	sources.push_back(*gsource);
//	sources.push_back(*fsource);
//
//	ReaderOptions options;
//	options.genNormals = true;
//	options.colorType = options.NORMAL_GRADIANT;
//	options.specifiedColor = glm::vec3(1.f, 0.f, 1.f);
//	boost::shared_ptr<GeometryInfo> geomInfo = ObjFileReader::readFile("C:\\VisualStudioProjects\\objmodels\\dragon.obj", options);
//
//	int r = 10, c = 10;
//	boost::shared_ptr<glm::mat4> matrices = getMatrices(geomInfo->bbox.getRadius(), r, c);
//	
//	boost::shared_ptr<TransformableGeometry> geom = boost::shared_ptr<TransformableGeometry>(new TransformableGeometry(GL_TRIANGLES, geomInfo->verticescolors,
//		geomInfo->indices, geomInfo->numVertices, geomInfo->numindices, geomInfo->vsize, geomInfo->csize, matrices, r*c, geomInfo->bbox));
//	geom->setNormals(geomInfo->normals, geomInfo->numNormals);
//
//	ShaderProgram* program = new ShaderProgram(sources);
//	boost::shared_ptr<ShaderParameters> shaderParams(new ShaderParameters());
//	g_meshParams = shaderParams;
//
//	UniformBufferObject lightUniformBuffer("LightInfo", 0);
//	lightUniformBuffer.addParameter4f("position", glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
//	lightUniformBuffer.addParameter3f("la", glm::vec3(0.5f, 0.5f, 0.5f));
//	lightUniformBuffer.addParameter3f("ld", glm::vec3(0.8f, 0.8f, 0.8f));
//	lightUniformBuffer.addParameter3f("ls", glm::vec3(0.8f, 0.8f, 0.8f));
//	UniformBufferObject materialUniformBuffer("MaterialInfo", 1);
//	materialUniformBuffer.addParameter3f("ka", glm::vec3(0.5f, 0.5f, 0.5f));
//	materialUniformBuffer.addParameter3f("kd", glm::vec3(0.8f, 0.8f, 0.8f));
//	materialUniformBuffer.addParameter3f("ks", glm::vec3(0.8f, 0.8f, 0.8f));
//	materialUniformBuffer.addParameter1f("shininess", 200.0f);
//	boost::shared_ptr<std::vector<UniformBufferObject>> uboVec = boost::shared_ptr<std::vector<UniformBufferObject>>(new std::vector<UniformBufferObject>());
//	g_uboParams = uboVec;
//	uboVec->push_back(lightUniformBuffer);
//	uboVec->push_back(materialUniformBuffer);
//		
//	program->setParameters(g_meshParams);
//	program->setUniformBuffers(uboVec);
//
//	g_scene = new Scene(g_canvasDim[0], g_canvasDim[1]);
//
//	GraphicObject* go1 = new GraphicObject(program);
//	go1->addGeometry(geom);
//	
//	g_scene->addGraphicObject(go1);
//	g_scene->viewAll();
//	BoundingBox wbox = g_scene->getWorldBox();
//	g_transZ = g_scene->getTranslationZ();
//	glClearColor(0.5f, 0.5f, 0.5f, 1.f);
//}
//
//void renderScene(void) {
//
//	g_scene->draw();
//
//	glutSwapBuffers();
//}
//
//static void processMouseClick(int button, int state, int x, int y) {
//	g_lastX = x;
//	g_lastY = y;
//
//	if (button == GLUT_LEFT_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_dragging = true;
//			g_button1Pressed = true;
//		}
//		else {
//			g_dragging = false;
//			g_button1Pressed = false;
//			g_viewAngle[0] += g_currViewAngle[0];
//			g_viewAngle[1] += g_currViewAngle[1];
//		}
//	}
//	else if (button == GLUT_MIDDLE_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_button2Pressed = true;
//			g_dragging = true;
//		}
//		else {
//			//float factor = g_scene->getWorldBox().getRadius()*0.10f;
//			g_button2Pressed = false;
//			g_dragging = false;
//			g_transZ += g_currtransZ;
//		}
//	}
//	else if (button == GLUT_RIGHT_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_dragging = true;
//			g_button3Pressed = true;
//		}
//		else {
//			g_dragging = false;
//			g_button3Pressed = false;
//			g_pan[0] += g_currPan[0];
//			g_pan[1] -= g_currPan[1];
//		}
//	}
//
//}
//
//static void mouseMove(int x, int y) {
//	float factor = 250.0f / (float)g_canvasDim[1];
//	float dx = factor * (x - g_lastX);
//	float dy = factor * (y - g_lastY);
//
//	if (g_button1Pressed && g_dragging) {
//		if (g_scene != 0) {
//				g_currViewAngle[0] = (float)((dx / (float)g_canvasDim[0]) * 360.0f);
//				g_currViewAngle[0] = g_currViewAngle[0] > 360.0f ? 0.0f : g_currViewAngle[0];
//
//				g_currViewAngle[1] = (float)((dy / (float)g_canvasDim[1]) * 360.0f);
//				g_currViewAngle[1] = g_currViewAngle[1] > 360.0f ? 0.0f : g_currViewAngle[1];
//
//			if (!g_enableManipulateLight) {
//				g_scene->rotateViewX(g_viewAngle[0] + g_currViewAngle[0]);
//				g_scene->rotateViewY(g_viewAngle[1] + g_currViewAngle[1]);
//			}
//			else {
//				glm::vec3 lightPos = glm::vec3(g_viewAngle[0] + g_currViewAngle[0], g_viewAngle[1] + g_currViewAngle[1], 1.0f);
//				lightPos = glm::normalize(lightPos);
//				std::cout << lightPos.x << " " << lightPos.y << " " << 1.0f << std::endl;
//				boost::shared_ptr<glm::vec4> prevLightPos = g_uboParams->at(0).getParameter4f("position");
//				prevLightPos->x = lightPos.x;
//				prevLightPos->y = lightPos.y;
//				prevLightPos->z = 1.0f;
//				g_uboParams->at(0).addParameter4f("position", *prevLightPos);
//				g_uboParams->at(0).dirty();
//			}
//		}
//	}
//	else if (g_button2Pressed && g_dragging) {
//		float factor = g_scene->getWorldBox().getRadius()*0.10f;
//
//		//std::cout << factor << std::endl;
//		g_currtransZ = ((y - g_lastY) / (float)g_canvasDim[1]);
//		g_currtransZ = g_currtransZ + (g_currtransZ*factor);
//		//cout << "setting transZ: " << g_transZ + g_currtransZ << endl;
//		if (g_scene != 0) {
//			g_scene->translationZ(g_transZ + g_currtransZ);
//		}
//	}
//	else if (g_button3Pressed && g_dragging) {
//		g_currPan[0] = (x - g_lastX) / (float)g_canvasDim[0];
//		g_currPan[1] = (y - g_lastY) / (float)g_canvasDim[1];
//		if (g_scene != 0) {
//			float factor = g_scene->getWorldBox().getRadius()*0.10;
//			g_currPan[0] = g_currPan[0] * factor;
//			g_currPan[1] = g_currPan[1] * factor;
//			g_scene->panX(g_pan[0] + g_currPan[0]);
//			g_scene->panY(g_pan[1] - g_currPan[1]);
//		}
//	}
//
//	glutPostRedisplay();
//}
//
//void reshape(int w, int h){
//	// Set the viewport to be the entire window
//	glViewport(0, 0, w, h);
//	if (g_scene != 0) {
//		g_scene->resize(w, h);
//
//		if (g_meshParams != NULL) {
//			g_meshParams->addParameter2f("u_viewport", glm::vec2(w, h));
//		}
//	}
//	glutPostRedisplay();
//}
//
//void processKeys(unsigned char key, int x, int y) {
//	if (g_scene == 0) {
//		return;
//	}
//
//	switch (key) {
//	case 'l':
//		g_enableManipulateLight = !g_enableManipulateLight;
//		std::cout << "Manipulate Light: " << g_enableManipulateLight << std::endl;
//		break;
//
//	case 'w':
//		if (g_meshParams != NULL) {
//			g_enableWireframe = !g_enableWireframe;
//			int val = g_enableWireframe == true ? 1 : 0;
//			g_meshParams->addParameter1i("u_enableWireframe", val);
//		}
//	}
//
//	glutPostRedisplay();
//}
//
//int main(int argc, char** argv) {
//	glutInit(&argc, argv);
//	glutInitWindowPosition(100, 100);
//	glutInitWindowSize(g_canvasDim[0], g_canvasDim[1]);
//	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DOUBLE);
//	glutInitContextVersion(4, 3);
//	//glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
//	glutInitContextFlags(GLUT_DEBUG);
//	int res = glutCreateWindow("GLTessExample");
//	glewExperimental = GL_TRUE;
//	GLenum err = glewInit();
//	if (err == GLEW_OK) {
//		initObjScene();
//		glutMouseFunc(processMouseClick);
//		glutMotionFunc(mouseMove);
//		glutKeyboardFunc(processKeys);
//		glutDisplayFunc(renderScene);
//		glutReshapeFunc(reshape);
//		glutMainLoop();
//	}
//	else {
//		fprintf(stderr, "Error in initializing 4.3 gl window");
//	}
//
//	return 0;
//}