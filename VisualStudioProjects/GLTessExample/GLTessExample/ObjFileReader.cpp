#include "ObjFileReader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <gtc\random.hpp>
#include <cfloat>
#include "BoundingBox.h"
#include <map>

ObjFileReader::ObjFileReader(){
}

boost::shared_ptr<GeometryInfo> ObjFileReader::readFile(std::string filepath, ReaderOptions& options) {
	std::ifstream validatefile(filepath);
	int numvertices=0;
	int numfaces=0;
	int facesize = 0;
	int vertexsize=0;

	std::clock_t start = std::clock();
	if (validatefile.is_open()) {
		std::string line;
		while (std::getline(validatefile, line)) {
			if (line[0] == '#') {
				continue;
			}
			if (line[0] == 'v') {
				++numvertices;
				if (vertexsize == 0) {
					std::istringstream iss(line);
					std::string vtoken;
					while (iss >> vtoken) {
						++vertexsize;
					}
					--vertexsize;
				}
			}
			else if (line[0] == 'f') {
				++numfaces;
				if (facesize == 0) {
					std::istringstream isstemp(line);
					std::string ftoken;
					while (isstemp >> ftoken) {
						++facesize;
					}
					--facesize;
				}
			}
		}
	}
	validatefile.close();
	std::clock_t end = std::clock();
	std::cout << "Time to validate obj file: " << (end - start) / CLOCKS_PER_SEC<<std::endl;
	std::cout << "NumVertices: " << numvertices << std::endl;
	std::cout << "Vertex Size: " << vertexsize << std::endl;
	std::cout << "NumFaces: " << numfaces << std::endl;
	std::cout << "Face Size: " << facesize << std::endl;

	bool storeverts, storeindices;
	int csize = options.genAlpha == true ? 4 : 3;
	GLfloat* poscolors = new GLfloat[(numvertices*3) + (numvertices*csize)];
	int numtrifaces = facesize == 4 ? numfaces * (facesize + 2) : numfaces * facesize;
	GLuint* indices = new GLuint[numtrifaces];
	
	glm::vec3 minpt = glm::vec3(FLT_MAX, FLT_MAX, FLT_MAX);
	glm::vec3 maxpt = glm::vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	glm::vec3 specifiedColor = options.specifiedColor;

	std::ifstream file(filepath);
	start = std::clock();
	if (file.is_open()) {

		std::string line;
		int vidx = 0, fidx = 0;
		while (std::getline(file, line)) {
			if (line[0] == '#') {
				continue;
			}
			if (line[0] == 'v') {
				std::istringstream iss(line);
				std::string vtoken;
				iss >> vtoken; //remove the 'v'
				for (int i = 0; i < 3; ++i) {
					iss >> vtoken;
					float pos = std::stof(vtoken);
					minpt[i] = minpt[i] > pos ? pos : minpt[i];
					maxpt[i] = maxpt[i] < pos ? pos : maxpt[i];
					poscolors[vidx++] = pos;
				}

				poscolors[vidx++] = specifiedColor.r;
				poscolors[vidx++] = specifiedColor.g;
				poscolors[vidx++] = specifiedColor.b;
				if (options.genAlpha) {
					poscolors[vidx++] = options.specifiedAlpha;
				}

			}
			else if (line[0] == 'f') {
				std::istringstream iss(line);
				std::string indexstr;
				iss >> indexstr; //remove the 'f'
				iss >> indexstr;
				GLuint prevfi0 = getFaceIndex(indexstr, 0);
				iss >> indexstr;
				GLuint prevfi1 = getFaceIndex(indexstr, 0);
				while(iss>>indexstr) {
					indices[fidx++] = prevfi0 - 1;
					indices[fidx++] = prevfi1 - 1;
				
					GLuint fi = getFaceIndex(indexstr, 0);
					indices[fidx++] = fi - 1;

					prevfi1 = fi;
				}
			}
		}

		GLfloat* normals = NULL;
		if (options.genNormals || options.colorType == options.NORMAL_GRADIANT) {
			normals = new GLfloat[numvertices * 3]();
			int startIdx = options.genAlpha == true ? 7 : 6;
			for (int i = 0; i < numtrifaces; i+=3) { //3 vertex index per triangle
				float v0x = poscolors[indices[i] * startIdx];
				float v0y = poscolors[indices[i] * startIdx + 1];
				float v0z = poscolors[indices[i] * startIdx + 2];

				float v1x = poscolors[indices[i + 1] * startIdx];
				float v1y = poscolors[indices[i + 1] * startIdx + 1];
				float v1z = poscolors[indices[i + 1] * startIdx + 2];

				float v2x = poscolors[indices[i + 2] * startIdx];
				float v2y = poscolors[indices[i + 2] * startIdx + 1];
				float v2z = poscolors[indices[i + 2] * startIdx + 2];

				glm::vec3 a = glm::vec3(v0x, v0y, v0z);
				glm::vec3 b = glm::vec3(v1x, v1y, v1z);
				glm::vec3 c = glm::vec3(v2x, v2y, v2z);

				glm::vec3 ba = b - a;
				glm::vec3 ca = c - a;
				glm::vec3 cross = glm::cross(ba, ca);

				normals[3 * indices[i]]         += cross.x;
				normals[3 * indices[i] + 1]     += cross.y;
				normals[3 * indices[i] + 2]     += cross.z;

				normals[3 * indices[i + 1]] += cross.x;
				normals[3 * indices[i + 1] + 1] += cross.y;
				normals[3 * indices[i + 1] + 2] += cross.z;

				normals[3 * indices[i + 2]] += cross.x;
				normals[3 * indices[i + 2] + 1] += cross.y;
				normals[3 * indices[i + 2] + 2] += cross.z;

			}
			for (int i = 0; i < numvertices; i++) {
				int idx = 3 * i;
				glm::vec3 normal = glm::vec3(normals[idx], normals[idx + 1], normals[idx + 2]);
				normal = glm::normalize(normal) * 0.5f + 0.5f;
				normals[idx] = normal.x;
				normals[idx + 1] = normal.y;
				normals[idx + 2] = normal.z; 
				//std::cout << "normal-"<<i<<" : "<< normal.x << " " << normal.y << " " << normal.z << std::endl;
			}
		}
		
		//setting up the bounds gradiant color.
		int startIdx = options.genAlpha == true ? 7 : 6; //3 coords per verts + 3 or 4 coords per color
		if (options.colorType == options.BOUNDS_GRADIANT) {
			
			float width = maxpt.x - minpt.x;
			float height = maxpt.y - minpt.y;
			float depth = maxpt.z - minpt.z;
			for (int i = 0; i < numvertices; ++i) {
				poscolors[startIdx * i + 3] = poscolors[startIdx * i] / width;
				poscolors[startIdx * i + 4] = poscolors[startIdx * i + 1] / height;
				poscolors[startIdx * i + 5] = poscolors[startIdx * i + 2] / depth;
				if (options.genAlpha) {
					poscolors[6 * i + 6] = options.specifiedAlpha;
				}
			}
		}
		else if (options.colorType == options.NORMAL_GRADIANT) {
			for (int i = 0; i < numvertices; ++i) {
				poscolors[startIdx * i + 3] = normals[3 * i];
				poscolors[startIdx * i + 4] = normals[3 * i + 1];
				poscolors[startIdx * i + 5] = normals[3 * i + 2];
				if (options.genAlpha) {
					poscolors[startIdx * i + 6] = options.specifiedAlpha;
				}
			}
		}
		end = std::clock();
		std::cout << "Time to read obj file: " << (end - start) / CLOCKS_PER_SEC << std::endl;
		BoundingBox bbox = BoundingBox(glm::vec4(minpt, 1.0f), glm::vec4(maxpt, 1.0f));
		boost::shared_ptr<GeomInfo> geomInfo = boost::shared_ptr<GeomInfo>(new GeomInfo());
		geomInfo->csize = options.genAlpha == true ? 4 : 3;
		geomInfo->vsize = 3;
		
		geomInfo->indices = boost::shared_ptr<GLuint>(indices);
		geomInfo->verticescolors = boost::shared_ptr<GLfloat>(poscolors);
		geomInfo->numindices = numtrifaces;
		geomInfo->numVertices = numvertices;
		geomInfo->drawmode = GL_TRIANGLES;
		geomInfo->isIndices = true;
		geomInfo->bbox = bbox;
		if (options.colorType == options.NORMAL_GRADIANT || options.genNormals) {
			geomInfo->normals = boost::shared_ptr<GLfloat>(normals);
			geomInfo->numNormals = numvertices;
		}

		return geomInfo;
	}
}

GLuint ObjFileReader::getFaceIndex(std::string& token, int shift) {
	std::size_t found = token.find('\\');
	if (found == std::string::npos) {
		return std::stoul(token);
	}

	std::string stok;
	std::istringstream iss(token);
	int count = 0;
	do {
		std::getline(iss, stok, '\\');
	} while (count <= shift);

	return std::stoul(stok);
}

void ObjFileReader::printVertsCols(GeometryInfo& ginfo) {
	GLfloat* poscolors = ginfo.verticescolors.get();
	int csize = ginfo.csize;
	int vsize = ginfo.vsize;
	int nverts = ginfo.numVertices;
	int totalsize = nverts * (csize + vsize);
	int attribstride = csize + vsize;
	std::cout << "Vertex Positions: " << std::endl;
	for (int i = 0; i < totalsize; i+=attribstride) {
		std::cout << poscolors[i] << " " << poscolors[i + 1] << " " << poscolors[i + 2] << std::endl;
	}
	std::cout << "Vertex Colors: " << std::endl;
	for (int i = vsize; i < totalsize; i+=attribstride) {
		if (csize == 3) {
			std::cout << poscolors[i] << " " << poscolors[i + 1] << " " << poscolors[i + 2] << std::endl;
		}
		else {
			std::cout << poscolors[i] << " " << poscolors[i + 1] << " " << poscolors[i + 2] << " "<<poscolors[i + 3]<<std::endl;
		}
	}

}

ObjFileReader::~ObjFileReader()
{
}
