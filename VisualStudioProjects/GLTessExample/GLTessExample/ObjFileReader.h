#pragma once
#include "TransformableGeometry.h"
#include <string>
#include <boost\shared_ptr.hpp>

struct ReaderOptions {
	enum ColorType {
		BOUNDS_GRADIANT,
		NORMAL_GRADIANT,
		SPECIFIED
	};
	ColorType colorType = SPECIFIED;
	glm::vec3 specifiedColor = glm::vec3(1.f, 1.f, 1.f);
	bool genAlpha = false;
	float specifiedAlpha = 0.5f;
	bool genNormals = false;
};

class ObjFileReader
{
private:
	ObjFileReader();
	static GLuint getFaceIndex(std::string& token, int shift);
	
public:
	static boost::shared_ptr<GeometryInfo> readFile(std::string filename, ReaderOptions& color);
	static void printVertsCols(GeometryInfo& ginfo);
	~ObjFileReader();
};

