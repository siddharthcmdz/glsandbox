#pragma once
#include <GL\glew.h>
#include <vector>
#include <glm.hpp>
#include <iostream>
#include <boost\shared_ptr.hpp>

typedef struct TubeInfo {
	boost::shared_ptr<GLfloat> vertices;
	GLfloat* radii;
	GLint numVertices;
	GLint numRadius;
	GLenum drawmode = GL_TRIANGLE_STRIP;
	GLint vsize = 3;
	GLint csize = 3;
	int numRings;
	bool colors;

	void print() const {
		std::cout << "num vertices: " << numVertices << " num radius: " << numRadius << " colors: " << colors << std::endl;
		for (int i = 0; i < numVertices; i++) {
			
			for (int j = 0; j < vsize + csize; j++) {
				int idx = i*(vsize + csize) + j;
				std::cout << vertices.get()[idx] << " ";
			}
			std::cout << std::endl;
		}
	}
} Tube;

class TubeData
{
private:
	TubeData();
	static glm::vec3 getPerp(const glm::vec3 vec);
public:
	static std::vector<glm::vec3> getRing(glm::vec3 pt, glm::vec3 vec, float radius, int numsamples);
	static std::vector<glm::vec3> getSquareRing(glm::vec3 pt, glm::vec3 vec, float radius, bool duplast);
	static boost::shared_ptr<Tube> createTubeArray(boost::shared_ptr<GLfloat> lineVerts, boost::shared_ptr<GLfloat> radii, int numLineVerts, int numringsamples, bool colors);
	~TubeData();
};


