#pragma once
#include "TransformableGeometry.h"
#include "ShaderProgram.h"
#include <vector>
#include <string>
#include "BoundingBox.h"
#include <boost\shared_ptr.hpp>
#include "OITParameters.h"

class GraphicObject
{
private:
	BoundingBox _wcbox;
	std::vector<boost::shared_ptr<TransformableGeometry>> _geomList;
	ShaderProgram* _program;
	glm::mat4 _vp;
	glm::mat4 _view;
	glm::mat4 _proj;
	boost::shared_ptr<OITParameters> _oitParams = NULL;
	bool _enableOIT;
	void updateBounds();
public:
	GraphicObject(ShaderProgram* program);
	void setOITParameter(boost::shared_ptr<OITParameters> oitParams);
	void enableOIT(bool onOff);
	const ShaderProgram* getShaderProgram() const;
	GLint getUniformLocation(const std::string uniform) const;
	BoundingBox getBounds() const;
	void addGeometry(boost::shared_ptr<TransformableGeometry> geom);
	void draw() const;
	void dispose();
	//void setViewProjection(const glm::mat4& vp);
	void setViewProjection(const glm::mat4& view, const glm::mat4& proj);
	~GraphicObject();
};

