#pragma once
#include <GL\glew.h>
#include <iostream>
#include <string>

class ShaderSource
{
private:
	std::string _source;
	GLenum _shadertype;
	GLuint _shaderId;

	bool init();
public:
	ShaderSource(std::string source, GLenum shadertype);
	GLuint getShaderId() const;
	void dispose() const;
	~ShaderSource();
};

