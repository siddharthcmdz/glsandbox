#version 430

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in gl_PerVertex {
	vec4 gl_Position;
} gl_in[];

uniform vec2 u_viewport = vec2(500.0f, 500.0f);
uniform int u_enableWireframe;

in vec3 v_color[3];
in vec3 v_normal[3];
in vec4 v_position[3];

out vec3 gv_color;
out vec3 gv_normal;
out vec4 gv_position;
noperspective out vec3 gv_dist;

void main() {
	vec3 dist0 = vec3(0, 0, 0);
	vec3 dist1 = vec3(0, 0, 0);
	vec3 dist2 = vec3(0, 0, 0);
	if(u_enableWireframe == 1) {
		vec2 p0 = u_viewport * gl_in[0].gl_Position.xy/gl_in[0].gl_Position.w;
		vec2 p1 = u_viewport * gl_in[1].gl_Position.xy/gl_in[1].gl_Position.w;
		vec2 p2 = u_viewport * gl_in[2].gl_Position.xy/gl_in[2].gl_Position.w;
		vec2 v0 = p2 - p1;
		vec2 v1 = p2 - p0;
		vec2 v2 = p1 - p0;
		float area = abs(v1.x * v2.y - v1.y * v2.x);
		
		float len0 = length(v0);
		dist0 = vec3(area/len0, 0.0f, 0.0f);

		float len1 = length(v1);
		dist1 = vec3(0.0f, area/len1, 0.0f);

		float len2 = length(v2);
		dist2 = vec3(0.0f, 0.0f, area/len2);
	}

	gl_Position = gl_in[0].gl_Position;
	gv_dist = dist0;
	gv_color = v_color[0];
	gv_normal = v_normal[0];
	gv_position = v_position[0];
	EmitVertex();

	gl_Position = gl_in[1].gl_Position;
	gv_dist = dist1;
	gv_color = v_color[1];
	gv_normal = v_normal[1];
	gv_position = v_position[1];
	EmitVertex();


	gl_Position = gl_in[2].gl_Position;
	gv_dist = dist2;
	gv_color = v_color[2];
	gv_normal = v_normal[2];
	gv_position = v_position[2];
	EmitVertex();

	EndPrimitive();
}