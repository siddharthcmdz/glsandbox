//#define FREEGLUT_LIB_PRAGMAS 0
//#include <GL\glew.h>
//#include <GL\freeglut.h>
//#include <iostream>
//#include "Geometry.h"
//#include "ShaderSource.h"
//#include "ShaderUtil.h"
//#include "ShaderProgram.h"
//#include "TransformableGeometry.h"
//#include "Scene.h"
//#include <algorithm>
//#include <vector>
//#include <gtc\matrix_transform.hpp>
//#include <gtc\random.hpp>
//#include "TubeData.h"
//#include "GLUtils.h"
//#include "ShaderParameters.h"
//#include <ctime>
//
//using namespace std;
//
//#define BUFFER_OFFSET(offset) (static_cast<GLuint*>(0) + (offset))
//
//Scene* g_scene;
//int g_lastX = -1, g_lastY = -1;
//bool g_dragging = false;
//bool g_button1Pressed = false;
//bool g_button2Pressed = false;
//bool g_button3Pressed = false;
//int g_canvasDim[2] = {500, 500};
//float g_currViewAngle[2] = {0.0f, 0.0f};
//float g_viewAngle[2] = { 0.0f, 0.0f };
//float g_transZ = 0.0f;
//float g_currtransZ = 0.0;
//float g_pan[2] = {0.0f, 0.0f};
//float g_currPan[2] = {0.0f, 0.0f};
//
//TransformableGeometry* g_patchGeom;
//boost::shared_ptr<ShaderParameters> g_tessParams;
//
//static Patch* getUrbsPatch(int nrows, int ncols, int npatchpertube) {
//	std::vector<std::vector<glm::vec3>> rings;
//	for (int i = 0; i < nrows; i++) {
//		for (int j = 0; j < ncols; j++) {
//			for (int k = 0; k < npatchpertube; k++) {
//				glm::vec3 start = glm::vec3(j*3, k*4-k, i*3);
//				for (int l = 0; l < 4; l++) {
//					std::vector<glm::vec3> ring = TubeData::getSquareRing(start, glm::vec3(0.0f, 1.0f, 0.0f), 1.0f, false);
//					start.y++;
//					rings.push_back(ring);
//				}
//			}
//		}
//	}
//	
//	GLfloat* vertices = new GLfloat[(rings.size() * rings[0].size()) * 3 * 2];
//	int k = 0;
//	glm::vec3 startcolor;
//	glm::vec3 endcolor;
//	int numringspertube = npatchpertube * 4;
//	for (size_t i = 0; i < rings.size(); i++) {
//		
//		std::vector<glm::vec3> ring = rings[i];
//		if (i % npatchpertube == 0) {
//			startcolor = glm::vec3(glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f));
//			endcolor = glm::vec3(glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f));
//		}
//		int start = i % numringspertube;
//		float mix = (float)(numringspertube - start) / (float)(numringspertube);
//		glm::vec3 color = startcolor * mix + (1 - mix)*endcolor;
//
//		for (size_t j = 0; j < ring.size(); j++) {
//			vertices[k++] = ring[j].x;
//			vertices[k++] = ring[j].y;
//			vertices[k++] = ring[j].z;
//
//			
//			vertices[k++] = color.r;
//			vertices[k++] = color.g;
//			vertices[k++] = color.b;
//		}
//	}
//
//	Patch* patch = new Patch();
//	patch->patchVertices = vertices;
//	patch->outerLevel[0] = 4;
//	patch->outerLevel[1] = 10;
//	patch->outerLevel[2] = 4;
//	patch->outerLevel[3] = 10;
//	patch->innerLevel[0] = 6;
//	patch->innerLevel[1] = 6;
//	patch->numPatches = rings.size()/4;
//	patch->numPatchVertices = 32;
//	patch->vsize = 3;
//	patch->csize = 3;
//	patch->patchtype = URBS;
//	patch->numTotalVertices = rings.size() * rings[0].size();
//
//	return patch;
//}
//
//static void initURBSPatchScene() {
//	ShaderSource* vsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\urbs.vert", GL_VERTEX_SHADER);
//	ShaderSource* tesource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\urbs.tcs", GL_TESS_CONTROL_SHADER);
//	ShaderSource* tcsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\urbsopt.tes", GL_TESS_EVALUATION_SHADER);
//	ShaderSource* gsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\urbs.gs", GL_GEOMETRY_SHADER);
//	ShaderSource* fsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\urbs.frag", GL_FRAGMENT_SHADER);
//	std::vector<ShaderSource> sources;
//	sources.push_back(*vsource);
//	sources.push_back(*tcsource);
//	sources.push_back(*tesource);
//	sources.push_back(*gsource);
//	sources.push_back(*fsource);
//
//	ShaderProgram* program = new ShaderProgram(sources);
//	boost::shared_ptr<ShaderParameters> shaderParams(new ShaderParameters());
//	g_tessParams = shaderParams;
//	g_tessParams->addParameter1i("tcs_iTessMode", 1);
//	program->setParameters(g_tessParams);
//	g_scene = new Scene(g_canvasDim[0], g_canvasDim[1]);
//
//	GraphicObject* go1 = new GraphicObject(program);
//
//	std::clock_t begin = clock();
//	Patch* patch = getUrbsPatch(100,100, 10);
//	std::clock_t end = clock();
//	std::cout << "Time to construct tubes: " << double(end - begin) / CLOCKS_PER_SEC<<std::endl;
//	g_patchGeom = new TransformableGeometry(GL_PATCHES, *patch);
//	boost::shared_ptr<TransformableGeometry> patchGeom = boost::shared_ptr<TransformableGeometry>(g_patchGeom);
//	go1->addGeometry(patchGeom);
//	g_scene->addGraphicObject(go1);
//	g_scene->viewAll();
//	BoundingBox wbox = g_scene->getWorldBox();
//	glm::vec3 wmin = glm::vec3(wbox.getMinPt().x, wbox.getMinPt().y, wbox.getMinPt().z);
//	glm::vec3 wmax = glm::vec3(wbox.getMaxPt().x, wbox.getMaxPt().y, wbox.getMaxPt().z);
//	g_tessParams->addParameter3f("tcs_wminpt", wmin);
//	g_tessParams->addParameter3f("tcs_wmaxpt", wmax);
//	g_transZ = g_scene->getTranslationZ();
//	glClearColor(0.5f, 0.5f, 0.5f, 1.f);
//}
//
//void renderScene(void) {
//
//	g_scene->draw();
//
//	glutSwapBuffers();
//}
//
//static void processMouseClick(int button, int state, int x, int y) {
//	g_lastX = x;
//	g_lastY = y;
//
//	if (button == GLUT_LEFT_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_dragging = true;
//			g_button1Pressed = true;
//		}
//		else {
//			g_dragging = false;
//			g_button1Pressed = false;
//			g_viewAngle[0] += g_currViewAngle[0];
//			g_viewAngle[1] += g_currViewAngle[1];
//		}
//	}
//	else if (button == GLUT_MIDDLE_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_button2Pressed = true;
//			g_dragging = true;
//		}
//		else {
//			//float factor = g_scene->getWorldBox().getRadius()*0.10f;
//			g_button2Pressed = false;
//			g_dragging = false;
//			g_transZ += g_currtransZ;
//		}
//	}
//	else if (button == GLUT_RIGHT_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_dragging = true;
//			g_button3Pressed = true;
//		}
//		else {
//			g_dragging = false;
//			g_button3Pressed = false;
//			g_pan[0] += g_currPan[0];
//			g_pan[1] -= g_currPan[1];
//		}
//	}
//	
//}
//
//static void mouseMove(int x, int y) {
//	float factor = 250.0f / (float)g_canvasDim[1];
//	float dx = factor * (x - g_lastX);
//	float dy = factor * (y - g_lastY);
//
//	if (g_button1Pressed && g_dragging) {
//		if (g_scene != 0) {
//			g_currViewAngle[0] = (float)((dx / (float)g_canvasDim[0]) * 360.0f);
//			g_currViewAngle[0] = g_currViewAngle[0] > 360.0f ? 0.0f : g_currViewAngle[0];
//
//			g_currViewAngle[1] = (float)((dy / (float)g_canvasDim[1]) * 360.0f);
//			g_currViewAngle[1] = g_currViewAngle[1] > 360.0f ? 0.0f : g_currViewAngle[1];
//
//			g_scene->rotateViewX(g_viewAngle[0] + g_currViewAngle[0]);
//			g_scene->rotateViewY(g_viewAngle[1] + g_currViewAngle[1]);
//		}
//	}
//	else if (g_button2Pressed && g_dragging) {
//		float factor = g_scene->getWorldBox().getRadius()*0.10f;
//
//		//std::cout << factor << std::endl;
//		g_currtransZ = ((y - g_lastY) / (float)g_canvasDim[1]);
//		g_currtransZ = g_currtransZ + (g_currtransZ*factor);
//		//cout << "setting transZ: " << g_transZ + g_currtransZ << endl;
//		if (g_scene != 0) {
//			g_scene->translationZ(g_transZ+ g_currtransZ);
//		}
//	}
//	else if (g_button3Pressed && g_dragging) {
//		g_currPan[0] = (x - g_lastX) / (float)g_canvasDim[0];
//		g_currPan[1] = (y - g_lastY) / (float)g_canvasDim[1];
//		if (g_scene != 0) {
//			g_scene->panX(g_pan[0] + g_currPan[0]);
//			g_scene->panY(g_pan[1] - g_currPan[1]);
//		}
//	}
//
//	glutPostRedisplay();
//}
//
//void reshape(int w, int h){
//	// Set the viewport to be the entire window
//	glViewport(0, 0, w, h);
//	if (g_scene != 0) {
//		g_scene->resize(w, h);
//	}
//	//if (g_tessParams != NULL) {
//	//	g_tessParams->addParameter2f("tcs_screensize", glm::vec2((float)w, (float)h));
//	//}
//	glutPostRedisplay();
//}
//
//void processKeys(unsigned char key, int x, int y) {
//	if (g_scene == 0) {
//		return;
//	}
//	switch (key) {
//	case '0': 
//	{
//		std::cout << "0 is pressed" << std::endl;
//		if (g_tessParams != NULL) {
//			g_tessParams->addParameter1i("tcs_iTessMode", 0);
//		}
//		break;
//	}
//	case '1':
//	{
//		std::cout << "1 is pressed" << std::endl;
//		if (g_tessParams != NULL) {
//			g_tessParams->addParameter1i("tcs_iTessMode", 1);
//		}
//		break;
//	}
//
//	case '2':
//	{
//		std::cout << "2 is pressed" << std::endl;
//		if (g_tessParams != NULL) {
//			g_tessParams->addParameter1i("tcs_iTessMode", 2);
//		}
//		break;
//	}
//
//	case 'n':
//	{
//		if (g_tessParams != NULL) {
//			boost::shared_ptr<glm::vec2> innerTess = g_tessParams->getParameter2f("tcs_tessInnerLevel");
//			innerTess->x = innerTess->x == 0 ? 1 : innerTess->x-1;
//			innerTess->y = innerTess->y == 0 ? 1 : innerTess->y-1;
//			g_tessParams->addParameter2f("tcs_tessInnerLevel", *innerTess);
//		}
//		break;
//	}
//
//	case 'j':
//	{
//		if (g_tessParams != NULL) {
//			boost::shared_ptr<glm::vec2> innerTess = g_tessParams->getParameter2f("tcs_tessInnerLevel");
//			innerTess->x = innerTess->x > 20 ? 20 : innerTess->x+1;
//			innerTess->y = innerTess->y > 20 ? 20 : innerTess->y+1;
//			g_tessParams->addParameter2f("tcs_tessInnerLevel", *innerTess);
//		}
//		break;
//	}
//
//	case 'k':
//	{
//		if (g_tessParams != NULL) {
//			boost::shared_ptr<glm::vec4> outerTess = g_tessParams->getParameter4f("tcs_tessOuterLevel");
//			if (outerTess->x == 0 || outerTess->y == 0 || outerTess->z == 0 || outerTess->w == 0) {
//				outerTess->x = 3;
//				outerTess->y = 3;
//				outerTess->z = 3;
//				outerTess->w = 3;
//			}
//			else {
//				outerTess->x = outerTess->x == 0 ? 1 : outerTess->x + 1;
//				outerTess->y = outerTess->y == 0 ? 1 : outerTess->y + 1;
//				outerTess->z = outerTess->z == 0 ? 1 : outerTess->z + 1;
//				outerTess->w = outerTess->w == 0 ? 1 : outerTess->w + 1;
//			}
//			g_tessParams->addParameter4f("tcs_tessOuterLevel", *outerTess);
//		}
//		break;
//	}
//
//	case 'm':
//	{
//		if (g_tessParams != NULL) {
//			boost::shared_ptr<glm::vec4> outerTess = g_tessParams->getParameter4f("tcs_tessOuterLevel");
//			if (outerTess->x == 0 || outerTess->y == 0 || outerTess->z == 0 || outerTess->w == 0) {
//				outerTess->x = 3;
//				outerTess->y = 3;
//				outerTess->z = 3;
//				outerTess->w = 3;
//			}
//			else {
//				outerTess->x = outerTess->x == 0 ? 1 : outerTess->x - 1;
//				outerTess->y = outerTess->y == 0 ? 1 : outerTess->y - 1;
//				outerTess->z = outerTess->z == 0 ? 1 : outerTess->z - 1;
//				outerTess->w = outerTess->w == 0 ? 1 : outerTess->w - 1;
//			}
//			g_tessParams->addParameter4f("tcs_tessOuterLevel", *outerTess);
//		}
//		break;
//	}
//
//	case 'w':
//	{
//		if (g_tessParams != NULL && g_patchGeom->getWireframeMode() == false) {
//			boost::shared_ptr<int> drawlines = g_tessParams->getParameter1i("fDrawLines");
//			if (*drawlines == -1 || *drawlines == 0) {
//				g_tessParams->addParameter1i("fDrawLines", 1);
//			}
//			else {
//				g_tessParams->addParameter1i("fDrawLines", 0);
//			}
//		}
//
//		break;
//	}
//
//	case 'q':
//	{
//		g_patchGeom->setWireframeMode(!g_patchGeom->getWireframeMode());
//		break;
//	}
//
//	}
//
//
//	glutPostRedisplay();
//}
//
//int main(int argc, char** argv) {
//	glutInit(&argc, argv);
//	glutInitWindowPosition(100, 100);
//	glutInitWindowSize(g_canvasDim[0], g_canvasDim[1]);
//	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DOUBLE);
//	glutInitContextVersion(4, 3);
//	//glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
//	glutInitContextFlags(GLUT_DEBUG);
//	int res = glutCreateWindow("GLTessExample");
//	glewExperimental = GL_TRUE;
//	GLenum err = glewInit();
//	if (err == GLEW_OK) {
//		initURBSPatchScene();
//		glutMouseFunc(processMouseClick);
//		glutMotionFunc(mouseMove);
//		glutKeyboardFunc(processKeys);
//		glutDisplayFunc(renderScene);
//		glutReshapeFunc(reshape);
//		glutMainLoop();
//	}
//	else {
//		fprintf(stderr, "Error in initializing 4.3 gl window");
//	}
//
//	return 0;
//}