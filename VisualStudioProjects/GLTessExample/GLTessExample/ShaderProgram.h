#pragma once
#include <GL\glew.h>
#include "ShaderSource.h"
#include <vector>
#include "ShaderParameters.h"
#include "UniformBufferObject.h"
#include <boost\shared_ptr.hpp>
#include "State.h"

class ShaderProgram
{
private:
	boost::shared_ptr<std::vector<boost::shared_ptr<ShaderSource>>> _sources;
	boost::shared_ptr<ShaderParameters> _params = NULL;
	boost::shared_ptr<std::vector<UniformBufferObject>> _ubos = NULL;
	boost::shared_ptr<std::vector<boost::shared_ptr<State>>>  _states = NULL;
	GLuint _programId;
	bool _dirty;
	bool init();

public:
	ShaderProgram(boost::shared_ptr<std::vector<boost::shared_ptr<ShaderSource>>> sources);
	void setParameters(boost::shared_ptr<ShaderParameters> params);
	void setUniformBuffers(boost::shared_ptr<std::vector<UniformBufferObject>> ubos);
	void addState(boost::shared_ptr<State> state);
	void apply() const;
	void unapply() const;
	bool isDirty() const;
	GLuint getProgramId() const;
	void dispose();
	~ShaderProgram();
};

