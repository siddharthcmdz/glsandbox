#include "ShaderSource.h"

ShaderSource::ShaderSource(std::string source, GLenum shadertype) {
	_source = source;
	_shadertype = shadertype;
	init();
}

bool ShaderSource::init() {
	_shaderId = glCreateShader(_shadertype);
	if (_shaderId <= 0) {
		fprintf(stderr, "Shader object creation failed");
		return false;
	}
	const char* c_str = _source.c_str();
	GLint length = strlen(c_str);
	glShaderSource(_shaderId, 1, &c_str, &length);
	glCompileShader(_shaderId);
	GLint success;
	glGetShaderiv(_shaderId, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar infoLog[1024];
		glGetShaderInfoLog(_shaderId, sizeof(infoLog), NULL, infoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", _shadertype, infoLog);
		return false;
	}
	return true;
}

void ShaderSource::dispose() const {
	glDeleteShader(_shaderId);
}

GLuint ShaderSource::getShaderId() const {
	return _shaderId;
}

ShaderSource::~ShaderSource() {
	dispose();
}
