#include "ShaderParameters.h"
#include <glm.hpp>

ShaderParameters::ShaderParameters() {
}

void ShaderParameters::addParameter1f(const std::string& str, const float value) {
	_u1fparams[str] = value;
}

void ShaderParameters::addParameter1i(const std::string& str, const int value) {
	_u1iparams[str] = value;
}

void ShaderParameters::addParameter2f(const std::string& str, const glm::vec2& value) {
	_u2fparams[str] = value;
}

void ShaderParameters::addParameter3f(const std::string& str, const glm::vec3& value) {
	_u3fparams[str] = value;
}

void ShaderParameters::addParameter4f(const std::string& str, const glm::vec4& value) {
	_u4fparams[str] = value;
}

boost::shared_ptr<glm::vec2> ShaderParameters::getParameter2f(const std::string str) const {
	U2FMAP::const_iterator iter = _u2fparams.find(str);
	if (iter == _u2fparams.end()) {
		return NULL;
	}
	return boost::shared_ptr<glm::vec2>(new glm::vec2(iter->second));
}

boost::shared_ptr<glm::vec3> ShaderParameters::getParameter3f(const std::string str) const {
	U3FMAP::const_iterator iter = _u3fparams.find(str);
	if (iter == _u3fparams.end()) {
		return NULL;
	}
	return boost::shared_ptr<glm::vec3>(new glm::vec3(iter->second));
}

boost::shared_ptr<glm::vec4> ShaderParameters::getParameter4f(const std::string str) const {
	U4FMAP::const_iterator iter = _u4fparams.find(str);
	if (iter == _u4fparams.end()) {
		return NULL;
	}
	return boost::shared_ptr<glm::vec4>(new glm::vec4(iter->second));
}

boost::shared_ptr<int> ShaderParameters::getParameter1i(const std::string str) const {
	U1IMAP::const_iterator iter = _u1iparams.find(str);
	if (iter == _u1iparams.end()) {
		return NULL;
	}
	
	return boost::shared_ptr<int>(new int[1]{iter->second});
}

boost::shared_ptr<float> ShaderParameters::getParameter1f(const std::string str) const {
	U1FMAP::const_iterator iter = _u1fparams.find(str);
	if (iter == _u1fparams.end()) {
		return NULL;
	}
	return boost::shared_ptr<float>(new float[1]{iter->second});
}

void ShaderParameters::apply(GLuint programId) {
	for (U1FMAP::iterator it = _u1fparams.begin(); it != _u1fparams.end(); ++it)	{
		GLint loc = getUniformLocation(programId, it->first);
		if (loc > -1) {
			glUniform1f(loc, it->second);
		}
	}

	for (U1IMAP::iterator it = _u1iparams.begin(); it != _u1iparams.end(); ++it) {
		GLint loc = getUniformLocation(programId, it->first);
		if (loc > -1) {
			glUniform1i(loc, it->second);
		}
	}

	for (U2FMAP::iterator it = _u2fparams.begin(); it != _u2fparams.end(); ++it) {
		GLint loc = getUniformLocation(programId, it->first);
		if (loc > -1) {
			glUniform2f(loc, it->second.x, it->second.y);
		}
	}

	for (U3FMAP::iterator it = _u3fparams.begin(); it != _u3fparams.end(); ++it) {
		GLint loc = getUniformLocation(programId, it->first);
		if (loc > -1) {
			glUniform3f(loc, it->second.x, it->second.y, it->second.z);
		}
	}

	for (U4FMAP::iterator it = _u4fparams.begin(); it != _u4fparams.end(); ++it) {
		GLint loc = getUniformLocation(programId, it->first); 
		if (loc > -1) {
			glUniform4f(loc, it->second.x, it->second.y, it->second.z, it->second.w);
		}
	}
}

boost::shared_ptr<std::vector<std::string>> ShaderParameters::getAllParameterNames() {
	std::vector<std::string>* keynames = new std::vector<std::string>();

	for (std::map<std::string, float>::iterator it = _u1fparams.begin(); it != _u1fparams.end(); ++it) {
		keynames->push_back(it->first);
	}
	for (std::map<std::string, int>::iterator it = _u1iparams.begin(); it != _u1iparams.end(); ++it) {
		keynames->push_back(it->first);
	}
	for (std::map<std::string, glm::vec2>::iterator it = _u2fparams.begin(); it != _u2fparams.end(); ++it) {
		keynames->push_back(it->first);
	}
	for (std::map<std::string, glm::vec3>::iterator it = _u3fparams.begin(); it != _u3fparams.end(); ++it) {
		keynames->push_back(it->first);
	}
	for (std::map<std::string, glm::vec4>::iterator it = _u4fparams.begin(); it != _u4fparams.end(); ++it) {
		keynames->push_back(it->first);
	}

	boost::shared_ptr<std::vector<std::string>> keynamesptr = boost::shared_ptr<std::vector<std::string>>(keynames);
	return keynamesptr;
}

int ShaderParameters::getUniformLocation(GLuint programId, const std::string& str) {
	GLint loc = glGetUniformLocation(programId, str.c_str());
	if (loc < 0) {
		fprintf(stderr, "Invalid uniform location : %d, %s\n", loc, str.c_str());
		return -1;
	}
	return loc;
}

ShaderParameters::~ShaderParameters()
{
}
