#define FREEGLUT_LIB_PRAGMAS 0
#include <GL\glew.h>
#include <GL\freeglut.h>
#include <iostream>
#include "Geometry.h"
#include "ShaderSource.h"
#include "ShaderUtil.h"
#include "ShaderProgram.h"
#include "TransformableGeometry.h"
#include "Scene.h"
#include <algorithm>
#include <vector>
#include <gtc\matrix_transform.hpp>
#include <gtc\random.hpp>
#include "TubeData.h"
#include "GLUtils.h"

using namespace std;

#define BUFFER_OFFSET(offset) (static_cast<GLuint*>(0) + (offset))

Scene* g_scene;
int g_lastX = -1, g_lastY = -1;
bool g_dragging = false;
bool g_button1Pressed = false;
bool g_button2Pressed = false;
bool g_button3Pressed = false;
int g_canvasDim[2] = { 500, 500 };
float g_currViewAngle[2] = { 0.0f, 0.0f };
float g_viewAngle[2] = { 0.0f, 0.0f };
float g_transZ = 0.0f;
float g_currtransZ = 0.0;
float g_pan[2] = { 0.0f, 0.0f };
float g_currPan[2] = { 0.0f, 0.0f };

TransformableGeometry* g_patchGeom;

GLfloat g_vertexData[] = {
	-0.5f, -0.5f, 0.0f, 1.f, 0.f, 0.f,
	0.0f, 0.5f, 0.0f, 0.f, 1.f, 0.f,
	0.5f, -0.5f, 0.0f, 0.f, 0.f, 1.f
};

GLfloat g_multiVertexData[] = {
	-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
	0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f,
	-0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,

	-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
	0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 0.0f,
	0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f
};

GLint g_startoffsets[] = { 0, 3 };

GLuint g_indices[] = { 0, 1, 2 };

GLfloat g_multiVertexData1[] = {
	-0.5f, 0.0f, 0.f, 0.f, 0.f, 1.f,
	0.0f, 0.0f, 0.f, 1.0, 0.f, 0.f,
	0.5f, 0.0f, 0.f, 1.f, 1.f, 1.f,
	0.0f, 0.5f, 0.f, 0.f, 1.f, 0.f
};

GLuint g_multiIndices[] = {
	3, 0, 1,
	3, 1, 2
};


GLsizei g_count[] = { 3, 3 };
const void* g_offsets[2] = { BUFFER_OFFSET(0 * sizeof(GLint)), BUFFER_OFFSET(3 * sizeof(GLint)) };

static boost::shared_ptr<float> getLineVerts(int numVerts) {
	boost::shared_ptr<float> verts = boost::shared_ptr<float>(new float[numVerts * 3]);
	for (int i = 0; i < numVerts; i++) {
		int idx = 3 * i;
		verts.get()[idx] = 0.f;
		verts.get()[idx + 1] = i;
		verts.get()[idx + 2] = 0.0f;
	}
	return verts;
}

static boost::shared_ptr<float> getRadii(int numVerts) {
	boost::shared_ptr<float> radii = boost::shared_ptr<float>(new float[numVerts]);
	for (int i = 0; i < numVerts; i++) {
		radii.get()[i] = 0.5f;
	}
	return radii;
}

static std::vector<TransformableGeometry*> createTubes(int rows, int cols, int tubelen, int numringsamples) {
	std::vector<TransformableGeometry*> geomList;
	boost::shared_ptr<float> lineVerts = getLineVerts(tubelen);
	
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			std::cout << "constructing: " << i << " " << j << std::endl;
			boost::shared_ptr<float> radii = getRadii(tubelen);
			boost::shared_ptr<Tube> tube = TubeData::createTubeArray(lineVerts, radii, tubelen, numringsamples, true);
			glm::vec3 trans = glm::vec3(i - ((float)rows / 2.0f), 0, j - ((float)cols / 2.0f));
			//glm::vec3 trans = glm::vec3(0, 0, 0);
			TransformableGeometry* geom = new TransformableGeometry(tube->drawmode, tube->vertices, tube->numVertices, tube->vsize, tube->csize);
			geom->setModelMatrix(glm::translate(glm::mat4(1.0f), trans));
			geomList.push_back(geom);

			tube->print();
		}
	}
	return geomList;
}

static std::vector<glm::vec3> createSquareTube(int numspinepts) {
	std::vector<glm::vec3> tube;
	for (int i = 0; i < numspinepts; i++) {
		std::vector<glm::vec3> ring = TubeData::getSquareRing(glm::vec3(0, i, 0), glm::vec3(0, 1, 0), 1, false);
		for (int i = 0; i < ring.size(); i++) {
			tube.push_back(ring[i]);
		}
	}
	return tube;
}

static TransformableGeometry* createEvalPoints(int resolution) {
	std::vector<glm::vec3> evalpts = getURBSPoints(2, resolution);
	//std::vector<glm::vec3> evalpts = TubeData::getSquareRing(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), 1);
	int* n = new int[1];
	GLfloat* verts = getGLPoints(evalpts, n);
	boost::shared_ptr<GLfloat> glvertices = boost::shared_ptr<GLfloat>(verts);
	TransformableGeometry* geom = new TransformableGeometry(GL_POINTS, glvertices, *n, 3, 3);
	return geom;
}

static void initScene() {
	boost::shared_ptr<ShaderSource> vsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\passthrough.vert", GL_VERTEX_SHADER);
	boost::shared_ptr<ShaderSource> fsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\passthrough.frag", GL_FRAGMENT_SHADER);
	boost::shared_ptr<std::vector<boost::shared_ptr<ShaderSource>>> sources = boost::shared_ptr<std::vector<boost::shared_ptr<ShaderSource>>>(new std::vector<boost::shared_ptr<ShaderSource>>());
	sources->push_back(vsource);
	sources->push_back(fsource);

	ShaderProgram* program = new ShaderProgram(sources);

	g_scene = new Scene(g_canvasDim[0], g_canvasDim[1]);

	////Create a single tube.
	//GLfloat* lineVerts = new GLfloat[9]{ 0.0f, 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f };
	//GLfloat* radii = new GLfloat[3] { 0.5f, 1.0f, 0.25f };
	//boost::shared_ptr<GLfloat> lineVertsptr = boost::shared_ptr<GLfloat>(lineVerts);
	//boost::shared_ptr<GLfloat> radiiptr = boost::shared_ptr<GLfloat>(radii);

	//boost::shared_ptr<Tube> tube = TubeData::createTubeArray(lineVertsptr, radiiptr, 3, true);
	//tube->print();
	//boost::shared_ptr<TransformableGeometry> geom = boost::shared_ptr<TransformableGeometry>(new TransformableGeometry(tube->drawmode, tube->vertices, tube->numVertices, tube->vsize, tube->csize));
	//geom->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
	//GraphicObject* go1 = new GraphicObject(program);
	//go1->addGeometry(geom);

	////Create transformable geometry with different ctors.
	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_vertexData, 3, 3, 3);
	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_multiVertexData, 6, 3, 3, g_startoffsets, g_count, 2);
	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_vertexData, g_indices, 3, 3, 3, 3);
	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_vertexData, g_multiIndices, 4, 6, 3, 3, g_count, 2);

	//std::vector<glm::vec3> ring = TubeData::getSquareRing(glm::vec3(0, 0, 0), glm::vec3(0, 1, 0), 1, false);
	//std::vector<glm::vec3> pts;
	//int nsamples = 15;
	//for (int i = 0; i < nsamples; i++) {
	//	float u = (float)i / (float)(nsamples - 1);
	//	glm::vec3 pt = evalURBV(u, ring);
	//	pts.push_back(pt);
	//}
	
	std::vector<glm::vec3> tube = createSquareTube(4);
	std::vector<glm::vec3> pts;
	int nusamples = 10;
	int nvsamples = 30;
	for (int v = 0; v < nvsamples; ++v) {
		float vparam = (float)v / (float)(nvsamples - 1);
		for (int u = 0; u < nusamples; ++u) {
			float uparam = (float)u / (float)(nusamples - 1);
			std::cout << uparam << " " << vparam << std::endl;
			glm::vec3 s = getURBSurfacePtOpt(uparam, vparam, tube);
			pts.push_back(s);
		}
	}

	int n;
	GLfloat* glpts = getGLPoints(pts, &n);
	boost::shared_ptr<GLfloat> glptsptr = boost::shared_ptr<GLfloat>(glpts);
	TransformableGeometry* geom = new TransformableGeometry(GL_POINTS, glptsptr, n, 3, 3);
	boost::shared_ptr<TransformableGeometry> geomptr = boost::shared_ptr<TransformableGeometry>(geom);

	GraphicObject* go1 = new GraphicObject(program);
	go1->addGeometry(geomptr);

	g_scene->addGraphicObject(go1);
	g_scene->viewAll();
	g_transZ = g_scene->getTranslationZ();
	glClearColor(0.5f, 0.5f, 0.5f, 1.f);
}

void renderScene(void) {

	g_scene->draw();

	glutSwapBuffers();
}

static void processMouseClick(int button, int state, int x, int y) {
	g_lastX = x;
	g_lastY = y;

	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			g_dragging = true;
			g_button1Pressed = true;
		}
		else {
			g_dragging = false;
			g_button1Pressed = false;
			g_viewAngle[0] += g_currViewAngle[0];
			g_viewAngle[1] += g_currViewAngle[1];
		}
	}
	else if (button == GLUT_MIDDLE_BUTTON) {
		if (state == GLUT_DOWN) {
			g_button2Pressed = true;
			g_dragging = true;
		}
		else {
			float factor = g_scene->getWorldBox().getRadius()*0.10f;
			g_button2Pressed = false;
			g_dragging = false;
			g_transZ += g_currtransZ;
		}
	}
	else if (button == GLUT_RIGHT_BUTTON) {
		if (state == GLUT_DOWN) {
			g_dragging = true;
			g_button3Pressed = true;
		}
		else {
			g_dragging = false;
			g_button3Pressed = false;
			g_pan[0] += g_currPan[0];
			g_pan[1] -= g_currPan[1];
		}
	}

}

static void mouseMove(int x, int y) {
	float factor = 250.0f / (float)g_canvasDim[1];
	float dx = factor * (x - g_lastX);
	float dy = factor * (y - g_lastY);

	if (g_button1Pressed && g_dragging) {
		if (g_scene != 0) {
			g_currViewAngle[0] = (float)((dx / (float)g_canvasDim[0]) * 360.0f);
			g_currViewAngle[0] = g_currViewAngle[0] > 360.0f ? 0.0f : g_currViewAngle[0];

			g_currViewAngle[1] = (float)((dy / (float)g_canvasDim[1]) * 360.0f);
			g_currViewAngle[1] = g_currViewAngle[1] > 360.0f ? 0.0f : g_currViewAngle[1];

			g_scene->rotateViewX(g_viewAngle[0] + g_currViewAngle[0]);
			g_scene->rotateViewY(g_viewAngle[1] + g_currViewAngle[1]);
		}
	}
	else if (g_button2Pressed && g_dragging) {
		float factor = g_scene->getWorldBox().getRadius()*0.10f;

		std::cout << factor << std::endl;
		g_currtransZ = ((y - g_lastY) / (float)g_canvasDim[1]);
		g_currtransZ = g_currtransZ + (g_currtransZ*factor);
		cout << "setting transZ: " << g_transZ + g_currtransZ << endl;
		if (g_scene != 0) {
			g_scene->translationZ(g_transZ + g_currtransZ);
		}
	}
	else if (g_button3Pressed && g_dragging) {
		g_currPan[0] = (x - g_lastX) / (float)g_canvasDim[0];
		g_currPan[1] = (y - g_lastY) / (float)g_canvasDim[1];
		if (g_scene != 0) {
			g_scene->panX(g_pan[0] + g_currPan[0]);
			g_scene->panY(g_pan[1] - g_currPan[1]);
		}
	}

	glutPostRedisplay();
}

void reshape(int w, int h){
	 //Set the viewport to be the entire window
	glViewport(0, 0, w, h);
	if (g_scene != 0) {
		g_scene->resize(w, h);
	}
	glutPostRedisplay();
}

void processKeys(unsigned char key, int x, int y) {
	if (g_scene == 0) {
		return;
	}
	
	switch (key) {
	case 'q':
	{
		g_patchGeom->setWireframeMode(!g_patchGeom->getWireframeMode());
		break;
	}

	}

	glutPostRedisplay();
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(g_canvasDim[0], g_canvasDim[1]);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DOUBLE);
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE); //tried to disable this for nsight debugging to do core profile as well, no effect
	glutInitContextFlags(GLUT_DEBUG);
	int res = glutCreateWindow("SimpleDriver");
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err == GLEW_OK) {
		initScene();
		glutMouseFunc(processMouseClick);
		glutMotionFunc(mouseMove);
		glutKeyboardFunc(processKeys);
		glutDisplayFunc(renderScene);
		glutReshapeFunc(reshape);
		glutMainLoop();
	}
	else {
		fprintf(stderr, "Error in initializing 4.2 gl window");
	}

	return 0;
}