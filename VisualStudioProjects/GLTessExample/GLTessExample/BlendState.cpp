#include "BlendState.h"

BlendState::BlendState() {}

BlendState::BlendState(GLenum srcFactor, GLenum dstFactor) : _srfFactor(srcFactor), _dstFactor(dstFactor) {

}

void BlendState::setBlendEquation(GLenum mode) {
	_mode = mode;
}

bool BlendState::apply() const {
	if (getEnable()) {
		glEnable(GL_BLEND);
		glBlendFunc(_srfFactor, _dstFactor);
		glBlendEquation(_mode);
	}

	return true;
}

bool BlendState::unapply() const {
	glDisable(GL_BLEND);

	return true;
}


BlendState::~BlendState() {
}
