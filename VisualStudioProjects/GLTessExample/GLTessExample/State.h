#pragma once
#include <gl\glew.h>
class State {
private:
	bool _enable;

protected:
	bool _dirty;

public:
	State();
	void setEnable(bool onOff);
	bool getEnable() const;
	void setDirty(bool onOff);
	bool getDirty() const;
	virtual bool apply() const = 0;
	virtual bool unapply() const = 0;
	~State();
};

