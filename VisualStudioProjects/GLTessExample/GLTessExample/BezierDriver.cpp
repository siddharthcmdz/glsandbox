//#define FREEGLUT_LIB_PRAGMAS 0
//#include <GL\glew.h>
//#include <GL\freeglut.h>
//#include <iostream>
//#include "Geometry.h"
//#include "ShaderSource.h"
//#include "ShaderUtil.h"
//#include "ShaderProgram.h"
//#include "TransformableGeometry.h"
//#include "Scene.h"
//#include <algorithm>
//#include <vector>
//#include <gtc\matrix_transform.hpp>
//#include <gtc\random.hpp>
//#include "TubeData.h"
//
//using namespace std;
//
//#define BUFFER_OFFSET(offset) (static_cast<GLuint*>(0) + (offset))
//
//Scene* g_scene;
//int g_lastX = -1, g_lastY = -1;
//bool g_dragging = false;
//bool g_button1Pressed = false;
//bool g_button2Pressed = false;
//bool g_button3Pressed = false;
//int g_canvasDim[2] = { 500, 500 };
//float g_currViewAngle[2] = { 0.0f, 0.0f };
//float g_viewAngle[2] = { 0.0f, 0.0f };
//float g_transZ = 0.0f;
//float g_currtransZ = 0.0;
//float g_pan[2] = { 0.0f, 0.0f };
//float g_currPan[2] = { 0.0f, 0.0f };
//
//TransformableGeometry* g_patchGeom;
//
//GLfloat g_vertexData[] = {
//	-0.5f, -0.5f, 0.0f, 1.f, 0.f, 0.f,
//	0.0f, 0.5f, 0.0f, 0.f, 1.f, 0.f,
//	0.5f, -0.5f, 0.0f, 0.f, 0.f, 1.f
//};
//
//GLfloat g_multiVertexData[] = {
//	-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
//	0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f,
//	-0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f,
//
//	-0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f,
//	0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 0.0f,
//	0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f
//};
//
//GLint g_startoffsets[] = { 0, 3 };
//
//GLuint g_indices[] = { 0, 1, 2 };
//
//GLfloat g_multiVertexData1[] = {
//	-0.5f, 0.0f, 0.f, 0.f, 0.f, 1.f,
//	0.0f, 0.0f, 0.f, 1.0, 0.f, 0.f,
//	0.5f, 0.0f, 0.f, 1.f, 1.f, 1.f,
//	0.0f, 0.5f, 0.f, 0.f, 1.f, 0.f
//};
//
//GLuint g_multiIndices[] = {
//	3, 0, 1,
//	3, 1, 2
//};
//
//
//GLsizei g_count[] = { 3, 3 };
//const void* g_offsets[2] = { BUFFER_OFFSET(0 * sizeof(GLint)), BUFFER_OFFSET(3 * sizeof(GLint)) };
//
//static float* getLineVerts(int numVerts) {
//	float* verts = new float[numVerts * 3];
//	for (int i = 0; i < numVerts; i++) {
//		int idx = 3 * i;
//		verts[idx] = 0.f;
//		verts[idx + 1] = i;
//		verts[idx + 2] = 0.0f;
//	}
//	return verts;
//}
//
//static float* getRadii(int numVerts) {
//	float* radii = new float[numVerts];
//	for (int i = 0; i < numVerts; i++) {
//		radii[i] = 0.5f;
//	}
//	return radii;
//}
//
//static std::vector<TransformableGeometry*> createTubes(int rows, int cols, int tubelen) {
//	std::vector<TransformableGeometry*> geomList;
//	float* lineVerts = getLineVerts(tubelen);
//
//	for (int i = 0; i < rows; i++) {
//		for (int j = 0; j < cols; j++) {
//			//std::cout << "constructing: " << i << " " << j << std::endl;
//			float* radii = getRadii(tubelen);
//			Tube* tube = TubeData::createTubeArray(lineVerts, radii, tubelen, true);
//			glm::vec3 trans = glm::vec3(i - ((float)rows / 2.0f), 0, j - ((float)cols / 2.0f));
//			//glm::vec3 trans = glm::vec3(0, 0, 0);
//			TransformableGeometry* geom = new TransformableGeometry(tube->drawmode, tube->vertices, tube->numVertices, tube->vsize, tube->csize);
//			geom->setModelMatrix(glm::translate(glm::mat4(1.0f), trans));
//			geomList.push_back(geom);
//
//			//tube->print();
//		}
//	}
//	return geomList;
//}
//
//static std::vector<std::vector<glm::vec3>> getRings(int numrings, int numsamples) {
//	std::vector<std::vector<glm::vec3>> rings;
//	for (int i = 0; i < numrings; i++) {
//		std::vector<glm::vec3> ring;
//		if (numsamples == 0) {
//			ring = TubeData::getRing(glm::vec3(0.0f, i, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 1.0f);
//		}
//		else {
//			ring = TubeData::getRing(glm::vec3(0.0f, i, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 1.0f, numsamples);
//		}
//		rings.push_back(ring);
//	}
//	return rings;
//}
//
//static Patch* getUrbsPatch() {
//	std::vector<std::vector<glm::vec3>> rings = getRings(2, 8);
//	GLfloat* vertices = new GLfloat[(rings.size() * rings[0].size()) * 3 * 2];
//	int k = 0;
//	for (size_t i = 0; i < rings.size(); i++) {
//		std::vector<glm::vec3> ring = rings[i];
//		for (size_t j = 0; j < ring.size(); j++) {
//			vertices[k++] = ring[j].x;
//			vertices[k++] = ring[j].y;
//			vertices[k++] = ring[j].z;
//
//			vertices[k++] = glm::linearRand(0.0f, 1.0f);
//			vertices[k++] = glm::linearRand(0.0f, 1.0f);
//			vertices[k++] = glm::linearRand(0.0f, 1.0f);
//		}
//	}
//
//	Patch* patch = new Patch();
//	patch->patchVertices = vertices;
//	patch->outerLevel[0] = 2;
//	patch->outerLevel[1] = 10;
//	patch->outerLevel[2] = 2;
//	patch->outerLevel[3] = 10;
//	patch->innerLevel[0] = 4;
//	patch->innerLevel[1] = 4;
//	patch->numPatchVertices = rings.size() * rings[0].size();
//	patch->vsize = 3;
//	patch->csize = 3;
//	patch->patchtype = URBS;
//	patch->numTotalVertices = rings.size() * rings[0].size();
//
//	return patch;
//}
//
//static Patch* getSimplePatch() {
//	std::vector<std::vector<glm::vec3>> rings = getRings(2, 0);
//	GLfloat* vertices = new GLfloat[(rings.size() * rings[0].size()) * 3 * 2];
//	int k = 0;
//	for (size_t i = 0; i < rings.size(); i++) {
//		std::vector<glm::vec3> ring = rings[i];
//		for (size_t j = 0; j < ring.size(); j++) {
//			vertices[k++] = ring[j].x;
//			vertices[k++] = ring[j].y;
//			vertices[k++] = ring[j].z;
//
//			vertices[k++] = glm::linearRand(0.0f, 1.0f);
//			vertices[k++] = glm::linearRand(0.0f, 1.0f);
//			vertices[k++] = glm::linearRand(0.0f, 1.0f);
//		}
//	}
//
//	Patch* patch = new Patch();
//	patch->patchVertices = vertices;
//	patch->outerLevel[0] = 2;
//	patch->outerLevel[1] = 10;
//	patch->outerLevel[2] = 2;
//	patch->outerLevel[3] = 10;
//	patch->innerLevel[0] = 4;
//	patch->innerLevel[1] = 4;
//	patch->numPatchVertices = rings.size() * rings[0].size();
//	patch->vsize = 3;
//	patch->csize = 3;
//	patch->numTotalVertices = rings.size() * rings[0].size();
//
//	return patch;
//}
//
//static void initPatchScene() {
//	ShaderSource* vsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\bezier.vert", GL_VERTEX_SHADER);
//	ShaderSource* tesource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\bezier.tes", GL_TESS_EVALUATION_SHADER);
//	ShaderSource* fsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\bezier.frag", GL_FRAGMENT_SHADER);
//	std::vector<ShaderSource> sources;
//	sources.push_back(*vsource);
//	sources.push_back(*tesource);
//	sources.push_back(*fsource);
//
//	ShaderProgram* program = new ShaderProgram(sources);
//
//	g_scene = new Scene(g_canvasDim[0], g_canvasDim[1]);
//
//	//Create a patch.
//	Patch* tubePatch = getSimplePatch();
//	g_patchGeom = new TransformableGeometry(GL_PATCHES, *tubePatch);
//	g_patchGeom->setEtch(GL_POINTS);
//	GraphicObject* go1 = new GraphicObject(program);
//	go1->addGeometry(g_patchGeom);
//
//	g_scene->addGraphicObject(go1);
//	g_scene->viewAll();
//	g_transZ = g_scene->getTranslationZ();
//	glClearColor(0.5f, 0.5f, 0.5f, 1.f);
//}
//
//static void initScene() {
//	ShaderSource* vsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\passthrough.vert", GL_VERTEX_SHADER);
//	ShaderSource* fsource = ShaderUtil::readShader("C:\\VisualStudioProjects\\GLTessExample\\GLTessExample\\passthrough.frag", GL_FRAGMENT_SHADER);
//	std::vector<ShaderSource> sources;
//	sources.push_back(*vsource);
//	sources.push_back(*fsource);
//
//	ShaderProgram* program = new ShaderProgram(sources);
//
//	g_scene = new Scene(g_canvasDim[0], g_canvasDim[1]);
//
//	//Create a single tube.
//	//GLfloat lineVerts[] = { 0.0f, 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f };
//	//GLfloat radii[] = { 0.5f, 1.0f, 0.25f };
//	//Tube* tube = TubeData::createTubeArray(lineVerts, radii, 3, true);
//	//tube->print();
//	//TransformableGeometry* geom = new TransformableGeometry(tube->drawmode, tube->vertices, tube->numVertices, tube->vsize, tube->csize);
//	//geom->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
//	//GraphicObject* go1 = new GraphicObject(program);
//	//go1->addGeometry(geom);
//
//	//Create transformable geometry with different ctors.
//	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_vertexData, 3, 3, 3);
//	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_multiVertexData, 6, 3, 3, g_startoffsets, g_count, 2);
//	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_vertexData, g_indices, 3, 3, 3, 3);
//	//TransformableGeometry* geom = new TransformableGeometry(GL_TRIANGLES, g_vertexData, g_multiIndices, 4, 6, 3, 3, g_count, 2);
//
//	//Create array of tubes.
//	std::vector<TransformableGeometry*> tubes = createTubes(10, 10, 10);
//	GraphicObject* go1 = new GraphicObject(program);
//	for (int i = 0; i < tubes.size(); i++) {
//		go1->addGeometry(tubes[i]);
//	}
//
//	g_scene->addGraphicObject(go1);
//	g_scene->viewAll();
//	g_transZ = g_scene->getTranslationZ();
//	glClearColor(0.5f, 0.5f, 0.5f, 1.f);
//}
//void renderScene(void) {
//
//	g_scene->draw();
//
//	glutSwapBuffers();
//}
//
//static void processMouseClick(int button, int state, int x, int y) {
//	g_lastX = x;
//	g_lastY = y;
//
//	if (button == GLUT_LEFT_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_dragging = true;
//			g_button1Pressed = true;
//		}
//		else {
//			g_dragging = false;
//			g_button1Pressed = false;
//			g_viewAngle[0] += g_currViewAngle[0];
//			g_viewAngle[1] += g_currViewAngle[1];
//		}
//	}
//	else if (button == GLUT_MIDDLE_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_button2Pressed = true;
//			g_dragging = true;
//		}
//		else {
//			g_button2Pressed = false;
//			g_dragging = false;
//			g_transZ += g_currtransZ;
//		}
//	}
//	else if (button == GLUT_RIGHT_BUTTON) {
//		if (state == GLUT_DOWN) {
//			g_dragging = true;
//			g_button3Pressed = true;
//		}
//		else {
//			g_dragging = false;
//			g_button3Pressed = false;
//			g_pan[0] += g_currPan[0];
//			g_pan[1] -= g_currPan[1];
//		}
//	}
//
//}
//
//static void mouseMove(int x, int y) {
//	float factor = 250.0f / (float)g_canvasDim[1];
//	float dx = factor * (x - g_lastX);
//	float dy = factor * (y - g_lastY);
//
//	if (g_button1Pressed && g_dragging) {
//		if (g_scene != 0) {
//			g_currViewAngle[0] = (float)((dx / (float)g_canvasDim[0]) * 360.0f);
//			g_currViewAngle[0] = g_currViewAngle[0] > 360.0f ? 0.0f : g_currViewAngle[0];
//
//			g_currViewAngle[1] = (float)((dy / (float)g_canvasDim[1]) * 360.0f);
//			g_currViewAngle[1] = g_currViewAngle[1] > 360.0f ? 0.0f : g_currViewAngle[1];
//
//			g_scene->rotateViewX(g_viewAngle[0] + g_currViewAngle[0]);
//			g_scene->rotateViewY(g_viewAngle[1] + g_currViewAngle[1]);
//		}
//	}
//	else if (g_button2Pressed && g_dragging) {
//		g_currtransZ = (y - g_lastY) / (float)g_canvasDim[1];
//		//cout << "setting transZ: " << g_transZ + g_currtransZ << endl;
//		if (g_scene != 0) {
//			g_scene->translationZ(g_transZ + g_currtransZ);
//		}
//	}
//	else if (g_button3Pressed && g_dragging) {
//		g_currPan[0] = (x - g_lastX) / (float)g_canvasDim[0];
//		g_currPan[1] = (y - g_lastY) / (float)g_canvasDim[1];
//		if (g_scene != 0) {
//			g_scene->panX(g_pan[0] + g_currPan[0]);
//			g_scene->panY(g_pan[1] - g_currPan[1]);
//		}
//	}
//
//	glutPostRedisplay();
//}
//
//void reshape(int w, int h){
//	// Set the viewport to be the entire window
//	glViewport(0, 0, w, h);
//	if (g_scene != 0) {
//		g_scene->resize(w, h);
//	}
//	glutPostRedisplay();
//}
//
//void processKeys(unsigned char key, int x, int y) {
//	if (g_scene == 0) {
//		return;
//	}
//	bool outerlevel = false;
//	bool innerlevel = false;
//	int idx = -1;
//	switch (key) {
//	case 'a':
//	{
//		outerlevel = true;
//		innerlevel = false;
//		idx = 0;
//		break;
//	}
//	case 's':
//	{
//		outerlevel = true;
//		innerlevel = false;
//		idx = 1;
//		break;
//	}
//
//	case 'd':
//	{
//		outerlevel = true;
//		innerlevel = false;
//		idx = 2;
//		break;
//	}
//
//	case 'w':
//	{
//		outerlevel = true;
//		innerlevel = false;
//		idx = 3;
//		break;
//	}
//
//	case 'n':
//	{
//		outerlevel = false;
//		innerlevel = true;
//		idx = 0;
//		break;
//	}
//
//	case 'm':
//	{
//		outerlevel = false;
//		innerlevel = true;
//		idx = 1;
//		break;
//	}
//
//	}
//
//	if (outerlevel) {
//		glm::vec4 ol = g_patchGeom->getPatchOuterLevel();
//		int mod = glutGetModifiers();
//		if (mod == GLUT_ACTIVE_ALT) {
//			ol[idx]--;
//		}
//		else {
//			ol[idx]++;
//		}
//		g_patchGeom->setPatchOuterLevel(ol);
//		std::cout << "patchOuterLevel[" << idx << "]: " << ol[idx] << std::endl;
//	}
//	else if (innerlevel) {
//		glm::vec2 il = g_patchGeom->getPatchInnerLevel();
//		int mod = glutGetModifiers();
//		if (mod == GLUT_ACTIVE_ALT) {
//			il[idx]--;
//		}
//		else {
//			il[idx]++;
//		}
//		g_patchGeom->setPatchInnerLevel(il);
//		std::cout << "patchInnerLevel[" << idx << "]: " << il[idx] << std::endl;
//	}
//
//	glutPostRedisplay();
//}
//
//int main(int argc, char** argv) {
//	glutInit(&argc, argv);
//	glutInitWindowPosition(100, 100);
//	glutInitWindowSize(g_canvasDim[0], g_canvasDim[1]);
//	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DOUBLE);
//	glutInitContextVersion(4, 2);
//	//	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
//	glutInitContextFlags(GLUT_DEBUG);
//	int res = glutCreateWindow("GLTessExample");
//	glewExperimental = GL_TRUE;
//	GLenum err = glewInit();
//	if (err == GLEW_OK) {
//		//initScene();
//		initPatchScene();
//		glutMouseFunc(processMouseClick);
//		glutMotionFunc(mouseMove);
//		glutKeyboardFunc(processKeys);
//		glutDisplayFunc(renderScene);
//		glutReshapeFunc(reshape);
//		glutMainLoop();
//	}
//	else {
//		fprintf(stderr, "Error in initializing 3.3 gl window");
//	}
//
//	return 0;
//}