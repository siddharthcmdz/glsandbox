#pragma once
#include <string>
#include "ShaderParameters.h"

class UniformBufferObject : public ShaderParameters {
private:
	std::string _name;
	bool _dirty;
	bool init(GLuint programId);
	GLuint _uboId = 0;
	int _base;

	boost::shared_ptr<std::vector<const char*>> convert(std::vector<std::string>& s);
protected:
	int getUniformLocation(GLuint programId, const std::string& str);
	
public:
	UniformBufferObject(const std::string& name, int binding);
	void apply(GLuint programId);
	void dirty();
	~UniformBufferObject();
};

