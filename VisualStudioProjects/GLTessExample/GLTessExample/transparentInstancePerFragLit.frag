#version 430

in vec4 v_color;
in vec3 v_normal;
in vec4 v_position;
layout( location = 0 ) out vec4 fragColor;

layout(early_fragment_tests) in;

layout(binding = 0) uniform LightInfo {
	vec4 position;
	vec3 la; //light ambient
	vec3 ld; //light diffuse
	vec3 ls; //light specular
}light;

layout(binding = 1) uniform MaterialInfo {
	vec3 ka; //ambient reflectivity
	vec3 kd; //diffuse reflectivity
	vec3 ks; //specular reflectivity
	float shininess; //specular shininess factor
} material;

#define MAX_FRAGMENTS 75

struct NodeType {
	vec4 color;
	float depth;
	uint next;
};

layout(binding = 0, r32ui) uniform uimage2D headPointers;
layout(binding = 0, offset=0) uniform atomic_uint nextNodeCounter;
layout(binding = 0, std430) buffer linkedLists {
	NodeType nodes[];
};
uniform uint MaxNodes;

subroutine void RenderPassType();
subroutine uniform RenderPassType RenderPass;

vec3 phongModel(vec4 eposition, vec3 enorm) {
	vec4 lposition = light.position;
	enorm = normalize(enorm);
	vec3 s = normalize(vec3(lposition - eposition));
	vec3 v = normalize(-eposition.xyz);
	vec3 r = reflect(-s, enorm);
	
	vec3 ambient = light.la * material.ka;
	float sdotn = max(dot(s, enorm), 0.0f);
	vec3 diffuse = light.ld * material.kd * sdotn;
	vec3 spec = vec3(0.0f);
	if(sdotn > 0.0f) {
		spec = light.ls * material.ks * pow(max(dot(r, v), 0.0f), material.shininess);
	}
	return ambient + diffuse + spec;
}


subroutine(RenderPassType)
void pass1() {
	//get the index of the next empty slot in the buffer.
	uint nodeIdx = atomicCounterIncrement(nextNodeCounter);

	//is there space left in the buffer?
	if(nodeIdx < MaxNodes) {
		uint prevHead = imageAtomicExchange(headPointers, ivec2(gl_FragCoord.xy), nodeIdx);

		vec3 lightIntensity = phongModel(v_position, v_normal);
		//blend between light and edge
		vec4 fragment = v_color * vec4(lightIntensity, 1.0f);

		nodes[nodeIdx].color = fragment;
		nodes[nodeIdx].depth = gl_FragCoord.z;
		nodes[nodeIdx].next = prevHead;
	}
}

subroutine(RenderPassType)
void pass2() {
	NodeType frags[MAX_FRAGMENTS];
	int count = 0;

	//Get the index of the head of the list.
	uint n = imageLoad(headPointers, ivec2(gl_FragCoord.xy)).r;

	//Copy the linked list for this fragment into an array.
	while(n != 0xffffffff && count < MAX_FRAGMENTS) {
		frags[count] = nodes[n];
		n = frags[count].next;
		count++;
	}

	//sort the array by depth using insertion sort(largest to smallest)
	for(uint i = 1; i < count; i++) {
		NodeType toInsert = frags[i];
		uint j = i;
		while(j > 0 && toInsert.depth > frags[j-1].depth) {
			frags[j] = frags[j-1];
			j--;
		}
		frags[j] = toInsert;
	}

	//traverse the array and combine the colors using the alpha channel.
	vec4 color = vec4(0.5f, 0.5f, 0.5f, 1.0f);
	for(int i = 0; i < count; i++) {
		color = mix(color, frags[i].color, frags[i].color.a);
	}

	fragColor = color;
}


/*
subroutine(RenderPassType)
void pass1() {
	fragColor=vec4(0.0f, 1.0, 0.0, 1.0f);
}

subroutine(RenderPassType)
void pass2() {
	fragColor=vec4(1.0f, 1.0, 0.0, 1.0f);
}
*/

void main() {
	RenderPass();
}