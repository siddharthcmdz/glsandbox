#pragma once
#include <gl\glew.h>
#include <glm.hpp>
#include "BoundingBox.h"
#include <vector>
#include <boost\shared_ptr.hpp>


enum PatchType { BEZIER, URBS, NURBS };

typedef struct PatchInfo {
	GLint numPatchVertices;
	GLfloat outerLevel[4];
	GLfloat innerLevel[2];
	GLfloat* patchVertices;
	GLint numTotalVertices;
	GLint vsize;
	GLint csize;
	PatchType patchtype;
	GLint numPatches;
	GLboolean tcsAvailable;
	PatchInfo() : numPatchVertices(0) {}
}Patch;

typedef struct GeometryInfo {
	GLenum drawmode;
	GLint numVertices;
	GLint vsize;
	GLint csize;
	boost::shared_ptr<GLfloat> verticescolors;
	boost::shared_ptr<GLuint> indices;
	boost::shared_ptr<GLfloat> normals;
	GLuint numNormals;
	GLint numindices;
	bool isIndices;
	BoundingBox bbox;
	glm::mat4 modelMat;
} GeomInfo;

class Geometry
{
private:
	BoundingBox _box;
	GLenum _drawmode;
	GLenum _etchmode = GL_POINTS;
	boost::shared_ptr<GLfloat> _data;
	int _numVerts;
	int _vsize;
	int _csize;
	GLuint _posBufferId;
	//for multiple prims
	GLint* _startOffsets;

	//for indices elements.
	boost::shared_ptr<GLuint> _indices;
	int _numIndices;
	GLuint _indicesBufferId;
	bool _useElements = false;

	//for multiple indices elements.
	int _primcount = 0;
	GLsizei* _counts;
	int _countsize;
	GLuint** _indicesOffset;

	//for patches
	Patch _patch;

	GLuint _vaoId;
	bool _dirtyPatchLevels;

	bool _wireframe = false;

	boost::shared_ptr<glm::mat4> _mats;
	int _numMats;
	GLuint _matInstanceId;

	boost::shared_ptr<GLfloat> _normals;
	int _numNormals;
	GLuint _normalBufferId;

protected:
	bool init();
	bool initNormals();
	void initBoundingBox();

public:
	Geometry(GLenum drawmode, Patch patch, const BoundingBox& box = BoundingBox());
	Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, const BoundingBox& box = BoundingBox());
	Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, GLint* startOffsets, GLsizei* counts, int primcount, const BoundingBox& box = BoundingBox());
	Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, const BoundingBox& box = BoundingBox());
	Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, boost::shared_ptr<glm::mat4> mats, int numMats, const BoundingBox& box = BoundingBox());
	Geometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, GLsizei* counts, int primcount, const BoundingBox& box = BoundingBox());
	void draw() const;
	void setWireframeMode(bool wireframe);
	bool getWireframeMode() const;
	void setPatchOuterLevel(const glm::vec4& outerlevel);
	void setPatchInnerLevel(const glm::vec2& innerlevel);
	glm::vec4 getPatchOuterLevel() const;
	glm::vec2 getPatchInnerLevel() const;
	void setNormals(boost::shared_ptr<GLfloat> normals, int numNormals);
	void dispose();
	BoundingBox getBoundingBox() const;
	~Geometry();
};

