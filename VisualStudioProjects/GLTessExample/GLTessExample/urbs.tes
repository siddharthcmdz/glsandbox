#version 400 core

layout(quads, equal_spacing, ccw) in;

uniform mat4 u_mvp;
in vec3 tcs_color[];
out vec3 tes_color;

out vec3 tesPosition;
out vec4 tesPatchDistance;

/**
* i = knot interval
* u = knot value
* p = degree
* U = knot vector
* Basis function evaluation for 12 knots , returns 3 basis(degree+1) non zero basis functions.
**/
float[3] URBSBasis_12_3(int i, float u, int p, float U[12]) {
	float N[3];
	N[0] = 1.0f;
	float left[3];
	float right[3];
	float saved = 0.0f;
	int j = 1;
	for (j = 1; j <= p; j++) {
		left[j] = u - U[i + 1 - j];
		right[j] = U[i + j] - u;
		saved = 0.0f;
		int r = 0;
		for (r = 0; r < j; r++) {
			float temp = N[r] / (right[r + 1] + left[j - r]);
			N[r] = saved + right[r + 1] * temp;
			saved = left[j - r] * temp;
		}
		N[j] = saved;
	}

	return N;
}

/**
* n + 1= number of Contol points
* p = degree of the curve
* u = [0, 1]
* U = knot vector
* returns knot span index for knot vector of 12 elements.
**/
int FindKnotSpan_12(int n, int p, float u, float U[12]) {
	if (u == U[n + 1]) {
		return n;
	}
	int low = p;
	int high = n + 1;
	int mid = (low + high) / 2;
	while (u < U[mid] || u >= U[mid+1]) {
		if (u < U[mid]) {
			high = mid;
		}
		else {
			low = mid;
		}
		mid = (low + high) / 2;
	}
	return mid;
}

//----------------------------------------------------------------------

/**
* n + 1= number of Contol points
* p = degree of the curve
* u = [0, 1]
* U = knot vector
* returns knot span index for knot vector of 6 elements.
**/
int FindKnotSpan_6(int n, int p, float u, float U[6]) {
	if (u == U[n + 1]) {
		return n;
	}
	int low = p;
	int high = n + 1;
	int mid = (low + high) / 2;
	while (u < U[mid] || u >= U[mid+1]) {
		if (u < U[mid]) {
			high = mid;
		}
		else {
			low = mid;
		}
		mid = (low + high) / 2;
	}
	return mid;
}

/**
* i = knot interval
* u = knot value
* p = degree
* U = knot vector
* Basis function evaluation for 6 knots , returns 2 basis(degree+1) non zero basis functions.
**/
float[2] URBSBasis_6_2(int i, float u, int p, float U[6]) {
	float N[2];
	N[0] = 1.0f;
	float left[2];
	float right[2];
	float saved = 0.0f;
	int j = 1;
	for (j = 1; j <= p; j++) {
		left[j] = u - U[i + 1 - j];
		right[j] = U[i + j] - u;
		saved = 0.0f;
		int r = 0;
		for (r = 0; r < j; r++) {
			float temp = N[r] / (right[r + 1] + left[j - r]);
			N[r] = saved + right[r + 1] * temp;
			saved = left[j - r] * temp;
		}
		N[j] = saved;
	}

	return N;
}

//---------------------------------------------------------------------------------

void main() {
	
	float v = gl_TessCoord.x;
	float u = gl_TessCoord.y;

	int pv = 2;
	float knotvector_v[12] = float[12]( 0.0f, 0.0f, 0.0f, 0.25f, 0.25f, 0.5f, 0.5F, 0.75f, 0.75f, 1.0f, 1.0f, 1.0f );
	float a = sqrt(2.0f) / 1.75f;
	float weights_v[9] = float[9](1.0f, a, 1.0f, a, 1.0f, a, 1.0f, a, 1.0f);

	int pu = 1;
	float knotvector_u[6] = float[6](0.0f, 0.0f, 0.33, 0.66, 1.0, 1.0f);
	float weights_u[4] = float[4](1.0f, 1.0f, 1.0f, 1.0f);


	//v is along x, u is along y.
	int vspan = FindKnotSpan_12(8, pv, v, knotvector_v); //8 = n-1 here n == 9
	float Nv[3] = URBSBasis_12_3(vspan, v, pv, knotvector_v);

	int uspan = FindKnotSpan_6(3, pu, u, knotvector_u); //3 = n-1 here n ==4
	float Nu[2] = URBSBasis_6_2(uspan, u, pu, knotvector_u);

	vec4 temp[4];
	vec3 ctemp[4];
	for(int l = 0; l <= pv; l++) {
		temp[l] = vec4(0.0f, 0.0f, 0.0f, 1.0f);
		ctemp[l] = vec3(0.0f, 0.0, 0.0f);
		for(int k = 0; k <= pu; k++) {
			int r = uspan-pu+k;
			int c = vspan-pv+l;
			int idx = r*8+(c % 8);
			//temp[l] = temp[l] + Nu[k] * weights_v[k] * gl_in[idx].gl_Position;
			temp[l] = temp[l] + Nu[k] * gl_in[idx].gl_Position;
			ctemp[l] = ctemp[l] + Nu[k] * tcs_color[idx];
		}
	}
	vec4 C = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	vec3 color = vec3(0.0f, 0.0f, 0.0f);
	for(int l = 0; l <= pv; l++) {
		C = C + Nv[l]*temp[l];
		color = color + Nv[l]*ctemp[l];
	}
	C /= C.w;
	
	tes_color = color;
	tesPosition = vec3(C.x, C.y, C.z);
	tesPatchDistance = vec4(u, v, 1-u, 1-v);
	gl_Position = u_mvp * C;
}