#include "Scene.h"
#include <gtc\matrix_transform.hpp>
#include <iostream>
#include <cmath>

#define PI 3.1415926535897f

Scene::Scene(int width, int height){
	_width = width;
	_height = height;
	_projection = glm::perspective<float>(45.0f, (float)width/(float)height, 0.01f, 1000.0f);
	_viewRot = glm::vec2(0, 0);
	_viewTranslate = glm::vec3(0, 0, 0);
	_worldBox = BoundingBox(glm::vec4(0), glm::vec4(0));
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
}

void Scene::resize(int width, int height) {
	_width = width;
	_height = height;
	_projection = glm::perspective<float>(45.0f, (float)width / (float)height, 0.1f, 1000.0f);
}

void Scene::viewAll() {
	if (_worldBox.isValid()) {
		float rad = _worldBox.getRadius();
		_viewTranslate.z = -rad;
		glm::vec4 center = _worldBox.getCenter();
		_viewTranslate.x = center.x;
		_viewTranslate.y = -center.y;
		updateViewMatricies();
	}
}

void Scene::addGraphicObject(GraphicObject* graphic) {
	_graphicsList.push_back(graphic);
	updateWorldBox();
}

void Scene::updateWorldBox() {
	for (size_t i = 0; i < _graphicsList.size(); i++) {
		BoundingBox box = _graphicsList[i]->getBounds();
		_worldBox.expandBy(box);
	}
}

GraphicObject* Scene::getGraphicObject(int idx) {
	return _graphicsList.at(idx);
}

BoundingBox Scene::getWorldBox() const {
	return _worldBox;
}

void Scene::draw() const {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (size_t i = 0; i < _graphicsList.size(); i++) {
		//std::cout << "Scene.draw() " << i << std::endl;
		GraphicObject* go = _graphicsList.at(i);
		//get the uniform location
		go->setViewProjection(_view, _projection);
		go->draw();
	}
}
void Scene::updateViewMatricies() {
	glm::mat4 viewTranslate = glm::translate(glm::mat4(1.0f), _viewTranslate);
	glm::mat4 viewRotationX = glm::rotate(viewTranslate, _viewRot.y, glm::vec3(-1.0f, 0.0f, 0.0f));
	_view = glm::rotate(viewRotationX, _viewRot.x, glm::vec3(0.0f, 1.0f, 0.0f));
}

void Scene::rotateViewX(float angle) {
	//std::cout << "rotate x: " << angle << std::endl;
	_viewRot.x = angle * (PI /180.0f);
	updateViewMatricies();
}

void Scene::rotateViewY(float angle) {
	//std::cout << "rotate y: " << angle << std::endl;
	_viewRot.y = angle * (PI / 180.0f);
	updateViewMatricies();
}

void Scene::translationZ(float z) {
	_viewTranslate.z = z;
	updateViewMatricies();
}

float Scene::getTranslationZ() const {
	return _viewTranslate.z;
}

void Scene::panX(float x)  {
	_viewTranslate.x = x;;
	updateViewMatricies();
}

void Scene::panY(float y) {
	_viewTranslate.y = y;
	updateViewMatricies();
}

void Scene::dispose() {
	for (size_t i = 0; i < _graphicsList.size(); i++) {
		GraphicObject* go = _graphicsList.at(i);
		go->dispose();
	}
}

Scene::~Scene(){
}
