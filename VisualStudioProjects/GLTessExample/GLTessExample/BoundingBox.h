#pragma once
#include <glm.hpp>

class BoundingBox
{
private:
	glm::vec4 _minPt;
	glm::vec4 _maxPt;

public:
	BoundingBox();
	BoundingBox(glm::vec4& minPt, glm::vec4& maxPt);
	BoundingBox(float minx, float miny, float minz, float maxx, float maxy, float maxz);
	void expandBy(const glm::vec4& pt);
	void expandBy(const BoundingBox& box);
	glm::vec4 getMinPt() const;
	glm::vec4 getMaxPt() const;
	glm::vec4 getCenter() const;
	float getRadius() const;
	bool isValid() const;
	~BoundingBox();
};

