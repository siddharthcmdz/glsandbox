#version 430
layout (location = 0) in vec3 vposition;
layout (location = 1) in vec3 vcolor;
layout (location = 2) in mat4 model;
layout (location = 6) in vec3 vnormal;

layout(binding = 0) uniform LightInfo {
	vec4 position;
	vec3 la; //light ambient
	vec3 ld; //light diffuse
	vec3 ls; //light specular
}light;

layout(binding = 1) uniform MaterialInfo {
	vec3 ka; //ambient reflectivity
	vec3 kd; //diffuse reflectivity
	vec3 ks; //specular reflectivity
	float shininess; //specular shininess factor
} material;

uniform mat4 u_mvp;
uniform mat4 u_view;

out vec3 v_color;
out vec3 v_normal;
flat out vec3 lightIntensity;

void getEyeSpace(out vec3 normal, out vec4 position) {
	mat4 mv = u_view * model;
	normal = normalize(mv * vec4(vnormal, 0.0)).xyz;
	position = mv * vec4(vposition, 1.0f);
}

vec3 phoneModel(vec4 eposition, vec3 enorm) {
	vec3 s = normalize(vec3(light.position - eposition));
	vec3 v = normalize(-eposition.xyz);
	vec3 r = reflect(-s, enorm);
	
	vec3 ambient = light.la * material.ka;
	float sdotn = max(dot(s, enorm), 0.0f);
	vec3 diffuse = light.ld * material.kd * sdotn;
	vec3 spec = vec3(0.0f);
	if(sdotn > 0.0) {
		spec = light.ls * material.ks * pow(max(dot(r, v), 0.0f), material.shininess);
	}
	return ambient + diffuse + spec;
	
}

void main() {
	vec3 eyeNorm ;
	vec4 eyePosition;
	getEyeSpace(eyeNorm, eyePosition);
	lightIntensity = phoneModel(eyePosition, eyeNorm);
	v_color = vcolor;
	gl_Position = u_mvp * model * vec4(vposition, 1f);
}