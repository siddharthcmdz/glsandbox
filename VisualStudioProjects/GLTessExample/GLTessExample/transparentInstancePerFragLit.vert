#version 430
layout (location = 0) in vec3 vposition;
layout (location = 1) in vec4 vcolor;
layout (location = 2) in mat4 model;
layout (location = 6) in vec3 vnormal;


uniform mat4 u_mvp;
uniform mat4 u_view;

out vec4 v_color;
out vec3 v_normal;
out vec4 v_position;

void main() {
	v_normal = normalize(u_view * model * vec4(vnormal, 0.0)).xyz;
	v_color = vcolor;
	v_position = u_view * model * vec4(vposition, 1.0f);
	gl_Position = u_mvp * model * vec4(vposition, 1.0f);
}