#include "GraphicObject.h"
#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>
#include <cfloat>
#include<gtc\matrix_access.hpp>

GraphicObject::GraphicObject(ShaderProgram* program) {
	_program = program;
}

void GraphicObject::addGeometry(boost::shared_ptr<TransformableGeometry> geom) {
	_geomList.push_back(geom);
	updateBounds();
}

const ShaderProgram* GraphicObject::getShaderProgram() const {
	return _program;
}

BoundingBox GraphicObject::getBounds() const {
	return _wcbox;
}

void GraphicObject::setOITParameter(boost::shared_ptr<OITParameters> oitParams) {
	_oitParams = oitParams;
}

void GraphicObject::enableOIT(bool onOff) {
	_enableOIT = onOff;
}

void GraphicObject::updateBounds() {
	glm::vec4 gomin = glm::vec4(FLT_MAX);
	glm::vec4 gomax = glm::vec4(-FLT_MAX);
	for (size_t i = 0; i < _geomList.size(); i++) {
		boost::shared_ptr<TransformableGeometry> geom = _geomList[i];
		glm::mat4 model = geom->getModelMatrix();
		BoundingBox box = geom->getBoundingBox();
		glm::vec4 min = box.getMinPt();
		glm::vec4 max = box.getMaxPt();
		glm::vec4 newmin = model * min;
		glm::vec4 newmax = model * max;
		
		gomin.x = newmin.x < gomin.x ? newmin.x : gomin.x; 
		gomin.y = newmin.y < gomin.y ? newmin.y : gomin.y;
		gomin.z = newmin.z < gomin.z ? newmin.z : gomin.z;

		gomax.x = newmax.x > gomax.x ? newmax.x : gomax.x;
		gomax.y = newmax.y > gomax.y ? newmax.y : gomax.y;
		gomax.z = newmax.z > gomax.z ? newmax.z : gomax.z;
	}

	_wcbox = BoundingBox(gomin, gomax);
}

void GraphicObject::draw() const {
	_program->apply();
	if (_oitParams != NULL && _oitParams->getDirty()) {
		_oitParams->init(_program->getProgramId());
		_oitParams->setDirty(false);
	}
	if (_oitParams != NULL) {
		_oitParams->clearBuffers(_program->getProgramId());
		_oitParams->pass1Start(_program->getProgramId());
	}
	GLint mvpMatrixLocation = getUniformLocation("u_mvp");
	if (mvpMatrixLocation < 0) {
		fprintf(stderr, "Info: MVP matrix not found in active shader program.\n");
	}

	GLuint viewMatrixLocation = getUniformLocation("u_view");
	if (viewMatrixLocation < 0) {
		fprintf(stderr, "Info: View matrix not found in active shader program.\n");
	}

	GLint eyePosLocation = getUniformLocation("u_eyePos");
	if (eyePosLocation < 0) {
		fprintf(stderr, "Info: Eye position not found in active shader program.\n");
	}

	for (size_t i = 0; i < _geomList.size(); i++) {
		if (mvpMatrixLocation >= 0) {
			glm::mat4 mvp = _proj * _view * _geomList[i]->getModelMatrix();
			glUniformMatrix4fv(mvpMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvp));
		}

		if (eyePosLocation >= 0) {
			glm::mat4 mv = _view * _geomList[i]->getModelMatrix();
			glm::mat4 vm = glm::inverse(mv);
			glm::vec4 eyepos = vm[3];
			//std::cout << "eye pos: " << eyepos.x << " " << eyepos.y << " " << eyepos.z << " " << eyepos.w <<" loc: "<<eyePosLocation<< std::endl;
			glUniform3f(eyePosLocation, eyepos.x, eyepos.y, eyepos.z);
		}

		if (viewMatrixLocation >= 0) {
			glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, glm::value_ptr(_view));
		}

		_geomList[i]->draw();
	}

	if (_oitParams != NULL) {
		_oitParams->pass1End(_program->getProgramId());
		_oitParams->pass2Start(_program->getProgramId());
		glm::mat4 identity = glm::mat4(1.0f);
		glUniformMatrix4fv(mvpMatrixLocation, 1, GL_FALSE, glm::value_ptr(identity));
		_oitParams->pass2End(_program->getProgramId());
	}

	_program->unapply();

	GLenum err;
	while ((err = glGetError()) != GL_NO_ERROR) {
		std::cerr << err << std::endl;
	}

}

GLint GraphicObject::getUniformLocation(const std::string uniform) const {
	return glGetUniformLocation(_program->getProgramId(), uniform.c_str());
}

//void GraphicObject::setViewProjection(const glm::mat4& vp) {
//	_vp = vp;
//}

void GraphicObject::setViewProjection(const glm::mat4& view, const glm::mat4& proj) {
	_view = view;
	_proj = proj;
}

void GraphicObject::dispose() {
	_program->dispose();
	for (size_t i = 0; i < _geomList.size(); i++) {
		_geomList.at(i)->dispose();
	}
}

GraphicObject::~GraphicObject() {
}
