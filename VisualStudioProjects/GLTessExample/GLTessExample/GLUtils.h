#pragma once

#include <iostream>
#include <vector>
#include <GL\glew.h>

/**
* i = knot interval
* u = knot value
* p = degree
* U = knot vector
**/
float* URBSBasis(int i, float u, int p, float* U) {
	float* N = new float[p+1];
	N[0] = 1.0f;
	float* left = new float[p+1];
	float* right = new float[p+1];
	float saved = 0.0f;
	int j = 1;
	for (j = 1; j <= p; j++) {
		left[j] = u - U[i + 1 - j];
		right[j] = U[i + j] - u;
		saved = 0.0f;
		int r = 0;
		for (r = 0; r < j; r++) {
			float temp = N[r] / (right[r + 1] + left[j - r]);
			N[r] = saved + right[r + 1] * temp;
			saved = left[j - r] * temp;
		}
		N[j] = saved;
	}
	delete left;
	delete right;

	return N;
}

/**
* n + 1= number of Contol points
* p = degree of the curve
* u = [0, 1]
* U = knot vector
**/
int FindKnotSpan(int n, int p, float u, float* U) {
	if (u == U[n + 1]) {
		return n;
	}
	int low = p;
	int high = n + 1;
	int mid = (low + high) / 2;
	while (u < U[mid] || u >= U[mid+1]) {
		if (u < U[mid]) {
			high = mid;
		}
		else {
			low = mid;
		}
		mid = (low + high) / 2;
	}
	return mid;
}

glm::vec3 getURBSPoint(std::vector<glm::vec3> pts, float p, float u, float* knotvector, float* weights) {
	int span = FindKnotSpan(pts.size()-1, p, u, knotvector);
	float* N = URBSBasis(span, u, p, knotvector);
	glm::vec4 C = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	for (int i = 0; i <= p; i++) {
		C = C + N[i] * weights[i] * glm::vec4(pts[span - p + i], 1.0f);
	}
	C = C / C.w;
	glm::vec3 pt = glm::vec3(C);
	return pt;
}

std::vector<glm::vec3> getURBSPoints(float p, int numsamples) {
	std::vector<glm::vec3> ring = TubeData::getSquareRing(glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f), 1.0f, false);
	//GLfloat knotvector[] = {0.0f, 0.0f, 0.0f, 0.166f, 0.33f, 0.5f, 0.66f, 0.833f, 1.0f, 1.0f, 1.0f};
	GLfloat knotvector[] = { 0.0f, 0.0f, 0.0f, 0.25f, 0.25f, 0.5f, 0.5F, 0.75f, 0.75f, 1.0f, 1.0f, 1.0f };
	GLfloat a = sqrt(2.0f) / 1.75f;
	GLfloat weights[] = {1.0f, a, 1.0f, a, 1.0f, a, 1.0f, a, 1.0f};
	std::vector<glm::vec3> evalpts;
	for (int i = 0; i < numsamples; i++) {
		float u = (float)i / (float)(numsamples - 1);
		glm::vec3 pt = getURBSPoint(ring, p, u, knotvector, weights);
		evalpts.push_back(pt);
	}
	
	return evalpts;
}

GLfloat* getGLPoints(std::vector<glm::vec3> pts, int *n) {
	GLfloat* verts = new GLfloat[pts.size() * 3*2];
	int k = 0;
	for (size_t i = 0; i < pts.size(); i++) {
		verts[k++] = pts[i].x;
		verts[k++] = pts[i].y;
		verts[k++] = pts[i].z;

		verts[k++] = glm::linearRand(0.0f, 1.0f);
		verts[k++] = glm::linearRand(0.0f, 1.0f);
		verts[k++] = glm::linearRand(0.0f, 1.0f);
	}
	*n = pts.size();
	return verts;
}

/**
* Basis functions for circular knots with 9 control points (8 with first and last same)
* U = {0, 0, 0, 0.25, 0.25, 0.5, 0.5, 0.75, 0.75, 1, 1, 1}
*/
void Bv(float v, float B[3], int spanidx[1]) {
	if (v >= 0.f && v < 0.25f) {
		B[0] = 16 * (0.25f - v)*(0.25f - v);
		B[1] = 32 * v*(0.25f - v);
		B[2] = 16 * v * v;
		spanidx[0] = 2;
	}
	else if (v >= 0.25f && v < 0.5f) {
		B[0] = 16 * (0.5f - v)*(0.5f - v);
		B[1] = 32 * (0.5f - v)*(v - 0.25f);
		B[2] = 16 * (v - 0.25f)*(v - 0.25f);
		spanidx[0] = 4;
	}
	else if (v >= 0.5f && v < 0.75f) {
		B[0] = 16 * (0.75f - v) * (0.75f - v);
		B[1] = 32 * (v - 0.5f)*(0.75f - v);
		B[2] = 16 * (v - 0.5f)*(v - 0.5f);
		spanidx[0] = 6;
	}
	else if (v >= 0.75f && v <= 1.0f) {
		B[0] = 16 * (1.0f - v)*(1.0f - v);
		B[1] = 32 * (v - 0.75f)*(1.0f - v);
		B[2] = 16 * (v - 0.75)*(v - 0.75f);
		spanidx[0] = 8;
	}
}

/**
* Basis functions for linear knots with 4 control points.
* U = {0, 0.2, 0.4, 0.6, 0.8, 1}
*/
void Bu(float u, float B[], int spanidx[1]) {
	float onethird = 1.0f / 3.0f;
	float onesixth = 2.0f * onethird;
	if (u >= 0.f && u < onethird) {
		B[0] = 3 * (onethird - u);
		B[1] = 3 * u;
		spanidx[0] = 0;
	}
	else if (u >= onethird && u < onesixth) {
		B[0] = 3 * (onesixth - u);
		B[1] = 3 * (u - onethird);
		spanidx[0] = 1;
	}
	else if (u >= onesixth && u <= 1) {
		B[0] = 3 * (1.0f - u);
		B[1] = 3 * (u - onesixth);
		spanidx[0] = 2;
	}
}

glm::vec3 evalURBV(float v, std::vector<glm::vec3> pts) {
	float u = v;
	glm::vec3 C;
	float B[3];
	int spanidx[1];
	Bv(v, B, spanidx);
	if (v >= 0.f && v < 0.25f) {
		C = (pts[0] * B[0]) + (pts[1] * B[1]) + (pts[2] * B[2]);
	}
	else if (v >= 0.25f && v < 0.5f) {
		C = (pts[2] * B[0]) + (pts[3] * B[1]) + (pts[4] * B[2]);
	}
	else if (v >= 0.5f && v < 0.75f) {
		C = (pts[4] * B[0]) + (pts[5] * B[1]) + (pts[6] * B[2]);
	}
	else if (v >= 0.75f && v <= 1.0f) {
		C = (pts[6] * B[0]) + (pts[7] * B[1]) + (pts[0] * B[2]);
	}
	return C;
}

glm::vec3 getURBSurfacePtOpt(float u, float v, std::vector<glm::vec3> tube) {
	float bvs[3];
	float bus[2];
	int uspanArr[1];
	int vspanArr[1];
	Bv(v, bvs, vspanArr);
	Bu(u, bus, uspanArr);
	int uspan = uspanArr[0];
	int vspan = vspanArr[0];
	int pu = 1;
	int pv = 2;
	glm::vec3 S;
	for (int l = 0; l <= pv; l++) {
		glm::vec3 temp;
		for (int k = 0; k <= pu; k++) {
			int r = uspan + k;
			int c = vspan - pv + l;
			int idx = r * 8 + (c % 8);
			temp = temp + bus[k]*tube[idx];
		}
		S = S + bvs[l] * temp;
	}
	return S;
}
