#pragma once
#include <vector>
#include <glm.hpp>
#include "GraphicObject.h"
#include "BoundingBox.h"

class Scene {
private:
	std::vector<GraphicObject*> _graphicsList;
	BoundingBox _worldBox;
	glm::mat4 _projection;
	glm::mat4 _view;
	glm::vec2 _viewRot;
	glm::vec3 _viewTranslate;

	int _width = 0, _height = 0;
	void updateViewMatricies();
	void updateWorldBox();

public:
	Scene(int width, int height);
	void rotateViewX(float angle);
	void rotateViewY(float angle);
	void panX(float transX);
	void panY(float transY);
	void translationZ(float z);

	float getTranslationZ() const;

	void resize(int width, int height);
	void addGraphicObject(GraphicObject* graphicObject);
	GraphicObject* getGraphicObject(int idx);
	void draw() const ;
	BoundingBox getWorldBox() const;
	void viewAll();
	void dispose();
	~Scene();
};

