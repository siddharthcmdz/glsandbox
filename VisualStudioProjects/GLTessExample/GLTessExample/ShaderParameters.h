#pragma once

#include <GL\glew.h>
#include <map>
#include <string>
#include <glm.hpp>
#include <boost\shared_ptr.hpp>
#include <vector>

//TODO: implement dirty mechanism for uniform parameters, currently they are set for every paint.
class ShaderParameters
{
private:
	typedef std::map<const std::string, glm::vec2> U2FMAP;
	typedef std::map<const std::string, glm::vec3> U3FMAP;
	typedef std::map<const std::string, glm::vec4> U4FMAP;
	typedef std::map<const std::string, int> U1IMAP;
	typedef std::map<const std::string, float> U1FMAP;
	
	U2FMAP _u2fparams;
	U4FMAP _u4fparams;
	U3FMAP _u3fparams;
	U1IMAP _u1iparams;
	U1FMAP _u1fparams;
	
protected:
	virtual int getUniformLocation(GLuint programId, const std::string& str);
public:
	ShaderParameters();
	void addParameter1f(const std::string& str, const float value);
	void addParameter1i(const std::string& str, const int value);
	void addParameter2f(const std::string& str, const glm::vec2& value);
	void addParameter3f(const std::string& str, const glm::vec3& value);
	void addParameter4f(const std::string& str, const glm::vec4& value);

	boost::shared_ptr<glm::vec2> getParameter2f(const std::string str) const;
	boost::shared_ptr<glm::vec3> getParameter3f(const std::string str) const;
	boost::shared_ptr<glm::vec4> getParameter4f(const std::string str) const;
	boost::shared_ptr<int> getParameter1i(const std::string str) const;
	boost::shared_ptr<float> getParameter1f(const std::string str) const;

	boost::shared_ptr<std::vector<std::string>> getAllParameterNames();
	virtual void apply(GLuint programId);
	~ShaderParameters();
};

