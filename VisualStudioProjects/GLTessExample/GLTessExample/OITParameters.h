#pragma once
#include <GL\glew.h>
#include "Geometry.h"
#include <boost\shared_ptr.hpp>

class OITParameters {

private:
	GLuint _counterBufferId;
	GLuint _llBufferId;
	GLuint _headPtrTexture;
	GLuint _clearBufferId;
	GLint _width, _height;
	GLuint _pass1Id;
	GLuint _pass2Id;
	boost::shared_ptr<Geometry> _quadGeom;
	bool _dirty = true;

public:
	OITParameters(GLint width, GLint height);
	bool init(GLuint programId);
	void clearBuffers(GLuint programId);
	void pass1Start(GLuint programId);
	void pass1End(GLuint programId);
	void pass2Start(GLuint programId);
	void pass2End(GLuint programId);
	bool getDirty() const;
	void setDirty(bool onOff);
	~OITParameters();
};

