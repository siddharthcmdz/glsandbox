#pragma once
#include "Geometry.h"
#include <glm.hpp>

class TransformableGeometry : public Geometry
{
private:
	glm::mat4 _model;
	void initMat();
public:
	void setModelMatrix(glm::mat4& modelMat);
	glm::mat4 getModelMatrix() const;

	TransformableGeometry(GLenum drawmode, Patch patch, const BoundingBox& box = BoundingBox());
	TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, const BoundingBox& box = BoundingBox());
	TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, int numVerts, int vsize, int csize, GLint* startOffsets, GLsizei* counts, int primcount, const BoundingBox& box = BoundingBox());
	TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, const BoundingBox& box = BoundingBox());
	TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, boost::shared_ptr<glm::mat4> mats, int numMats, const BoundingBox& box = BoundingBox());
	TransformableGeometry(GLenum drawmode, boost::shared_ptr<GLfloat> data, boost::shared_ptr<GLuint> indices, int numVerts, int numIndices, int vsize, int csize, int* counts, int primcount, const BoundingBox& box = BoundingBox());
	void draw() const ;
	~TransformableGeometry();
};

