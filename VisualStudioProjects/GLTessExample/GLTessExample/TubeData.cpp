#include "TubeData.h"
#include <GL\glew.h>
#include <glm.hpp>
#include <gtx\rotate_vector.hpp>
#include <gtx\perpendicular.hpp>
#include <gtc\random.hpp>
#include <vector>

#define PI 3.14

TubeData::TubeData(){}

glm::vec3 TubeData::getPerp(const glm::vec3 vec) {
	glm::vec3 v0 = glm::vec3(vec.y, -vec.x, 0);
	glm::vec3 v1 = glm::vec3(-vec.z, 0, vec.x);

	return v0 + v1;
}

std::vector<glm::vec3> TubeData::getRing(glm::vec3 pt, glm::vec3 vec, float radius, int numsamples) {
	glm::vec3 up = glm::vec3(vec);
	glm::vec3 v0 = glm::normalize(getPerp(up));
	glm::vec3 v1 = glm::cross(v0, up);
	v1 = glm::normalize(v1);

	std::vector<glm::vec3> ring;
	
	for (int i = 0; i < numsamples-1; i++) {
		float angle = (float)i / (float)(numsamples - 1) * 360.f;
		angle = angle * (PI / 180.0f);
		glm::vec3 rpt = pt + v0 * cos(angle) + v1 * sin(angle);
		ring.push_back(rpt);
	}
	ring.push_back(ring[0]);

	return ring;
}

std::vector<glm::vec3> TubeData::getSquareRing(glm::vec3 pos, glm::vec3 vec, float radius, bool duplast) {
	glm::vec3 up = glm::vec3(vec);
	glm::vec3 v0 = glm::normalize(getPerp(up));
	glm::vec3 v1 = glm::cross(v0, up);
	v1 = glm::normalize(v1);

	std::vector<glm::vec3> ring;

	for (int i = 0; i < 8; i++) {
		float angle = (float)i / 8.0f * 360.f;
		angle = angle * (PI / 180.0f);
		glm::vec3 pt = v0 * cos(angle) + v1 * sin(angle);
		if (i % 2 == 1) {
			float len = sqrt(radius * radius + radius * radius);
			pt = glm::normalize(pt);
			pt = pt*len;
		}
		pt += pos;
		ring.push_back(pt);
	}
	if (duplast) {
		ring.push_back(ring[0]);
	}

	return ring;
}

boost::shared_ptr<Tube> TubeData::createTubeArray(boost::shared_ptr<GLfloat> lineVerts, boost::shared_ptr<GLfloat> radii, int numLineVerts, int numringsamples, bool colors) {
	int cols = colors == true ? 2 : 1;
	GLfloat* verts = new GLfloat[((numLineVerts-1)*2*5*3) * cols]; //(num edges * 2 (upper and lower verts) * 5 samples * 3 coords) * 2 for colors
	std::vector<glm::vec3> prevring;
	glm::vec3 startcolor = glm::vec3(glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f));
	glm::vec3 endcolor = glm::vec3(glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f));
	int k = 0;

	for (int i = 0; i < numLineVerts; i++) {
		int i0 = i * 3;
		glm::vec3 p0 = glm::vec3(lineVerts.get()[i0], lineVerts.get()[i0 + 1], lineVerts.get()[i0 + 2]);
		if (i == 0) {
			int i1 = i*3+3;
			glm::vec3 p1 = glm::vec3(lineVerts.get()[i1], lineVerts.get()[i1 + 1], lineVerts.get()[i1 + 2]);
			prevring = getRing(p0, glm::normalize(p0 - p1), radii.get()[i], numringsamples);
			continue;
		}
		int i1 = i*3-3;
		glm::vec3 p1 = glm::vec3(lineVerts.get()[i1], lineVerts.get()[i1 + 1], lineVerts.get()[i1 + 2]);
		std::vector<glm::vec3> currring = getRing(p0, glm::normalize(p1 - p0), radii.get()[i], numringsamples);
		
		float prevmix = (float)(i-1) / (float)(numLineVerts - 1);
		float currmix = (float)i / (float)(numLineVerts - 1);
		for (size_t i = 0; i < currring.size(); i++) {
			glm::vec3 currcolor = glm::mix(startcolor, endcolor, currmix);
			glm::vec3 prevcolor = glm::mix(startcolor, endcolor, prevmix);

			verts[k++] = currring[i].x;
			verts[k++] = currring[i].y;
			verts[k++] = currring[i].z;

			verts[k++] = currcolor.x;
			verts[k++] = currcolor.y;
			verts[k++] = currcolor.z;

			verts[k++] = prevring[i].x;
			verts[k++] = prevring[i].y;
			verts[k++] = prevring[i].z;

			verts[k++] = prevcolor.x;
			verts[k++] = prevcolor.y;
			verts[k++] = prevcolor.z;
		}
		prevring = currring;
	}

	boost::shared_ptr<Tube> tube = boost::shared_ptr<Tube>(new Tube());
	tube->drawmode = GL_TRIANGLE_STRIP;
	tube->numRadius = numLineVerts;
	tube->numVertices = (numLineVerts - 1) * 2 * 5;
	tube->radii = radii.get();
	tube->vertices = boost::shared_ptr<GLfloat>(verts);
	tube->vsize = 3;
	tube->csize = colors == true ? 3 : 0;
	tube->colors = colors;

	return tube;
}

TubeData::~TubeData(){}
