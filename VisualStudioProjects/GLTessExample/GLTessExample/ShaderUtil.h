#pragma once

#include "ShaderSource.h"
#include <iostream>
#include <fstream>
#include <string>
#include <GL\glew.h>
#include <boost\shared_ptr.hpp>

class ShaderUtil {
private:
	ShaderUtil();
public:
	static boost::shared_ptr<ShaderSource> readShader(const std::string filepath, GLenum shadertype);
};

boost::shared_ptr<ShaderSource> ShaderUtil::readShader(const std::string filepath, GLenum shadertype) {
	std::ifstream file(filepath);
	std::string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

	//std::ifstream t("file.txt");
	//std::stringstream buffer;
	//buffer << t.rdbuf();

	return boost::shared_ptr<ShaderSource>(new ShaderSource(str, shadertype));
}
