#include "ShaderProgram.h"
#include <iostream>

ShaderProgram::ShaderProgram(boost::shared_ptr<std::vector<boost::shared_ptr<ShaderSource>>> sources) {
	_sources = sources;
	init();
}

GLuint ShaderProgram::getProgramId() const {
	return _programId;
}

void ShaderProgram::setParameters(boost::shared_ptr<ShaderParameters> params) {
	_params = params;
}

void ShaderProgram::setUniformBuffers(boost::shared_ptr<std::vector<UniformBufferObject>> ubos) {
	_ubos = ubos;
}

bool ShaderProgram::init() {
	_programId = glCreateProgram();
	if (_programId <= 0) {
		fprintf(stderr, "Shader program creation failed");
		return false;
	}
	
	for (size_t i = 0; i < _sources->size(); i++) {
		int shaderId = _sources->at(i)->getShaderId();
		glAttachShader(_programId, shaderId);
	}
	glLinkProgram(_programId);

	int success = 0;
	glGetProgramiv(_programId, GL_LINK_STATUS, &success);
	if (success == 0) {
		char errorLog[1024];
		glGetProgramInfoLog(_programId, sizeof(errorLog), NULL, errorLog);
		fprintf(stderr, "Error linking shader program: '%s'\n", errorLog);
		return false;
	}
	glValidateProgram(_programId);
	return true;
}

void ShaderProgram::apply() const {
	glUseProgram(_programId);
	if (_params != NULL) {
		_params->apply(_programId);
	}
	if (_ubos != NULL) {
		for (size_t i = 0; i < _ubos->size(); i++) {
			_ubos->at(i).apply(_programId);
		}
	}
	if (_states != NULL) {
		for (size_t i = 0; i < _states->size(); i++) {
			_states->at(i)->apply();
		}
	}
}

void ShaderProgram::unapply() const {
	glUseProgram(0);
	if (_states != NULL) {
		for (size_t i = 0; i < _states->size(); i++) {
			_states->at(i)->unapply();
		}
	}
}

void ShaderProgram::addState(boost::shared_ptr<State> state) {
	if (_states == NULL) {
		_states = boost::shared_ptr<std::vector<boost::shared_ptr<State>>>(new std::vector<boost::shared_ptr<State>>());
	}
	_states->push_back(state);
}

void ShaderProgram::dispose() {
	for (size_t i = 0; i < _sources->size(); i++) {
		_sources->at(i)->dispose();
	}
	glDeleteProgram(_programId);
}

ShaderProgram::~ShaderProgram() {
	dispose();
}
