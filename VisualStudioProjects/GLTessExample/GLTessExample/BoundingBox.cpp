#include "BoundingBox.h"
#include <math.h>

#define EPSILON 0.0000001f

BoundingBox::BoundingBox() : _minPt(NAN), _maxPt(NAN) {}

BoundingBox::BoundingBox(glm::vec4& minPt, glm::vec4& maxPt) : _minPt(minPt), _maxPt(maxPt) {
}

BoundingBox::BoundingBox(float minx, float miny, float minz, float maxx, float maxy, float maxz) {
	_minPt = glm::vec4(minx, miny, minz, 1.0f);
	_maxPt = glm::vec4(maxx, maxy, maxz, 1.0f);
}

void BoundingBox::expandBy(const glm::vec4& pt) {
	_minPt.x = pt.x < _minPt.x ? pt.x : _minPt.x;
	_minPt.y = pt.y < _minPt.y ? pt.y : _minPt.y;
	_minPt.z = pt.z < _minPt.z ? pt.z : _minPt.z;
	_minPt.z = 1.0f;

	_maxPt.x = pt.x > _maxPt.x ? pt.x : _maxPt.x;
	_maxPt.y = pt.y > _maxPt.y ? pt.y : _maxPt.y;
	_maxPt.z = pt.z > _maxPt.z ? pt.z : _maxPt.z;
	_maxPt.z = 1.0f;
}

void BoundingBox::expandBy(const BoundingBox& box) {
	expandBy(box.getMinPt());
	expandBy(box.getMaxPt());
}

float BoundingBox::getRadius() const {
	glm::vec4 diag = _maxPt - _minPt;
	return glm::length(diag);
}

glm::vec4 BoundingBox::getMinPt() const {
	return _minPt;
}

glm::vec4 BoundingBox::getMaxPt() const {
	return _maxPt;
}
glm::vec4 BoundingBox::getCenter() const {
	float half = 0.5f;
	return glm::mix(_maxPt, _minPt, 0.5f);
}

bool BoundingBox::isValid() const {
	return !isnan(_minPt.x) && !isnan(_minPt.y) && !isnan(_minPt.z) &&
		!isnan(_maxPt.x) && !isnan(_maxPt.y) && !isnan(_maxPt.z);
}

BoundingBox::~BoundingBox()
{
}
