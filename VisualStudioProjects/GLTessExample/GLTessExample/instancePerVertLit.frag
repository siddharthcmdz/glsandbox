#version 430

in vec3 v_color;
out vec4 fragColor;

flat in vec3 lightIntensity;

void main() {
    fragColor = vec4(v_color * lightIntensity, 1f);
}