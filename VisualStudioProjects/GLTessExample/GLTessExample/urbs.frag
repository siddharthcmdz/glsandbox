#version 400 core

out vec4 fragColor;

in vec3 gTriDistance;
in vec4 gPatchDistance;
in vec3 gv_color;

uniform int fDrawLines = 0;


float amplify(float d, float scale, float offset) {
	d = scale * d + offset;
	d = clamp(d, 0, 1);
	d = 1 - exp2(-2*d*d);
	return d;
}

void main() {
	vec3 color = vec3(1.0f, 1.0f, 1.0f);
	
	if(fDrawLines == 1) {
		color = vec3(0.5f, 0.5f, 0.5f);
		vec3 innerlinecolor = vec3(1.0f, 1.0f, 1.0f);
		float d1 = min(min(gTriDistance.x, gTriDistance.y), gTriDistance.z);
		float d2 = min(min(min(gPatchDistance.x, gPatchDistance.y), gPatchDistance.z), gPatchDistance.w);
		d1 = 1 - amplify(d1, 50, -0.5f);
		d2 = amplify(d2, 50, -0.5f);
		color = d2 * color + d1*d2*innerlinecolor;
	} 

	fragColor = vec4(gv_color*color, 1f);
	
}