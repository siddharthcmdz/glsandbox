#version 430

in vec3 gv_color;
in vec3 gv_normal;
in vec4 gv_position;
noperspective in vec3 gv_dist;
out vec4 fragColor;

layout(binding = 0) uniform LightInfo {
	vec4 position;
	vec3 la; //light ambient
	vec3 ld; //light diffuse
	vec3 ls; //light specular
}light;

layout(binding = 1) uniform MaterialInfo {
	vec3 ka; //ambient reflectivity
	vec3 kd; //diffuse reflectivity
	vec3 ks; //specular reflectivity
	float shininess; //specular shininess factor
} material;

uniform int u_enableWireframe;

vec3 phoneModel(vec4 eposition, vec3 enorm) {
	vec4 lposition = light.position;
	enorm = normalize(enorm);
	vec3 s = normalize(vec3(lposition - eposition));
	vec3 v = normalize(-eposition.xyz);
	vec3 r = reflect(-s, enorm);
	
	vec3 ambient = light.la * material.ka;
	float sdotn = max(dot(s, enorm), 0.0f);
	vec3 diffuse = light.ld * material.kd * sdotn;
	vec3 spec = vec3(0.0f);
	if(sdotn > 0.0) {
		spec = light.ls * material.ks * pow(max(dot(r, v), 0.0f), material.shininess);
	}
	return ambient + diffuse + spec;
}


void main() {
	float nearD = min(min(gv_dist.x, gv_dist.y), gv_dist.z) * u_enableWireframe;
	float edgeIntensity = 0.0f;
	if (abs(nearD) > 0.001f && abs(nearD) < 0.99f) {
		edgeIntensity = exp2(-1.0f * nearD * nearD);
		vec3 lightIntensity = phoneModel(gv_position, gv_normal);
		fragColor = vec4(edgeIntensity, 0.0f, 0.0f, 1.0f) * vec4(lightIntensity, 1.0f) * vec4(gv_color, 1.0f) * gl_FragCoord.z;
	}
	else {

	vec3 lightIntensity = phoneModel(gv_position, gv_normal);

	//blend between light and edge
	fragColor = vec4(gv_color, 1.0f) * vec4(lightIntensity, 1.0f);
	}

}