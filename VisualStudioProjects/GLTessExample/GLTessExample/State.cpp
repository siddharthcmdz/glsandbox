#include "State.h"


State::State() {
}

void State::setEnable(bool onOff) {
	_enable = onOff;
}

bool State::getEnable() const {
	return _enable;
}

void State::setDirty(bool onOff) {
	_dirty = onOff;
}

bool State::getDirty() const {
	return _dirty;
}

State::~State(){
}