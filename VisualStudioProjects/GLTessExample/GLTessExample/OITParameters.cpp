#include "OITParameters.h"
#include <cstddef>
#include <vector>
#include <iostream>

OITParameters::OITParameters(GLint width, GLint height) : _width(width), _height(height) {
}

bool OITParameters::getDirty() const {
	return _dirty;
}

void OITParameters::setDirty(bool onOff) {
	_dirty = onOff;
}

bool OITParameters::init(GLuint programId) {
	_pass1Id = glGetSubroutineIndex(programId, GL_FRAGMENT_SHADER, "pass1");
	_pass2Id = glGetSubroutineIndex(programId, GL_FRAGMENT_SHADER, "pass2");
	GLfloat* verticesColors = new GLfloat[24] {
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.8f,
		-1.0f,  1.0f, 0.0f, 0.0f, 0.0f, 0.8f,
		 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.8f,
		 1.0f,  1.0f, 0.0f, 0.0f, 0.0f, 0.8f,
		
	};
	boost::shared_ptr<GLfloat> quadGeom = boost::shared_ptr<GLfloat>(verticesColors);
	
	_quadGeom = boost::shared_ptr<Geometry>(new Geometry(GL_TRIANGLE_STRIP, quadGeom, 4, 3, 3));

	int nodesize = 5 * sizeof(GLfloat) + sizeof(GLuint);
	int maxNodes = 20 * _width * _height;
	
	//atomic counter buffer
	glGenBuffers(1, &_counterBufferId);
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, _counterBufferId); //target, index, bufferid
	glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint), NULL, GL_DYNAMIC_DRAW); 

	//the buffer for the head pointers, as an image texture.
	glGenTextures(1, &_headPtrTexture);
	glBindTexture(GL_TEXTURE_2D, _headPtrTexture);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32UI, _width, _height);
	glBindImageTexture(0, _headPtrTexture, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32UI);

	//the buffer of linked list.
	glGenBuffers(1, &_llBufferId);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, _llBufferId);
	glBufferData(GL_SHADER_STORAGE_BUFFER, maxNodes * nodesize, NULL, GL_DYNAMIC_DRAW);
	
	GLuint maxNodesloc = glGetUniformLocation(programId, "MaxNodes");
	if (maxNodesloc >= 0) {
		glUniform1ui(maxNodesloc, maxNodes);
	}

	std::vector<GLuint>* headPtrClear = new std::vector<GLuint>(_width* _height, 0xffffffff);
	glGenBuffers(1, &_clearBufferId);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _clearBufferId);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, headPtrClear->size()*sizeof(GLuint), headPtrClear->data(), GL_STATIC_COPY);

	return true;
}

void OITParameters::clearBuffers(GLuint programId) {
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _clearBufferId);
	glBindTexture(GL_TEXTURE_2D, _headPtrTexture);
	glTextureSubImage2D(GL_TEXTURE_2D, 0, 0, 0, _width, _height, GL_RED_INTEGER, GL_UNSIGNED_INT, NULL);

	GLuint zero = 0;
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, _counterBufferId);
	glBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0,sizeof(GLuint), &zero);
}

void OITParameters::pass1Start(GLuint programId) {
	glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &_pass1Id);
	GLuint maxNodesloc = glGetUniformLocation(programId, "MaxNodes");
	if (maxNodesloc >= 0) {
		int maxNodes = 20 * _width * _height;
		glUniform1ui(maxNodesloc, maxNodes);
	}
	else {
		std::cout << "Max nodes not found in shader" << std::endl;
	}

	glDepthMask(GL_FALSE);
}

void OITParameters::pass1End(GLuint programId) {
	glFinish();
}

void OITParameters::pass2Start(GLuint programId) {
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &_pass2Id);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OITParameters::pass2End(GLuint programId) {
	_quadGeom->draw();
}


OITParameters::~OITParameters() {
	std::cout << "destoying oit parameters" << std::endl;
}
