#include "UniformBufferObject.h"
#include <vector>

UniformBufferObject::UniformBufferObject(const std::string& name, int base) : _name(name), _base(base) {
}

int UniformBufferObject::getUniformLocation(GLuint programId, const std::string& str) {
	return -1;
}

boost::shared_ptr<std::vector<const char*>> UniformBufferObject::convert(std::vector<std::string>& s)
{
	boost::shared_ptr<std::vector<const char*>> chararr = boost::shared_ptr<std::vector<const char*>>(new std::vector<const char*>());
	chararr->reserve(s.size()*_name.size()+2);
	for (int i = 0; i < s.size(); i++) {
		std::string name = (_name+ "." + s[i]);
		char* namechar = new char[name.size() + 1];
		std::strcpy(namechar, name.c_str());
		chararr->push_back(namechar);
	}
	return chararr;
}

bool UniformBufferObject::init(GLuint programId) {
	GLuint blockIdx = glGetUniformBlockIndex(programId, _name.c_str());

	//query for the block size.
	GLint blockSize;
	glGetActiveUniformBlockiv(programId, blockIdx, GL_UNIFORM_BLOCK_DATA_SIZE, &blockSize);

	//alloc mem for block.
	GLubyte* blockBuffer = new GLubyte[blockSize]();
	
	//Query offset of each variable in the block. Get the index of each variable withing the block.
	boost::shared_ptr<std::vector<std::string>> paramnames = getAllParameterNames();
	//duplicate and append the ubo name.
	boost::shared_ptr<std::vector<const char*>> paramnamesptr = convert(*paramnames);

	int numParams = paramnames->size();
	GLuint* indices = new GLuint[numParams]();
	glGetUniformIndices(programId, numParams, &paramnamesptr->at(0), indices);

	GLint* offsets = new GLint[numParams]();
	glGetActiveUniformsiv(programId, numParams, indices, GL_UNIFORM_OFFSET, offsets);

	//place the data into the buffer at the appropriate offsets
	for (int i = 0; i < numParams; i++) {
		std::string str = paramnames->at(i);
		if (getParameter1f(str) != NULL) {
			boost::shared_ptr<float> val = getParameter1f(str);
			memcpy(blockBuffer + offsets[i], val.get(), sizeof(float));
		}
		else if (getParameter1i(str) != NULL) {
			boost::shared_ptr<int> val = getParameter1i(str);
			memcpy(blockBuffer + offsets[i], val.get(), sizeof(int));
		}
		else if (getParameter2f(str) != NULL) {
			boost::shared_ptr<glm::vec2> val = getParameter2f(str);
			float* valptr = &val.get()->x;
			memcpy(blockBuffer + offsets[i], valptr, 2 * sizeof(float));
		}
		else if (getParameter3f(str) != NULL) {
			boost::shared_ptr<glm::vec3> val = getParameter3f(str);
			float* valptr = &val.get()->x;
			memcpy(blockBuffer + offsets[i], valptr, 3 * sizeof(float));

		}
		else if (getParameter4f(str) != NULL) {
			boost::shared_ptr<glm::vec4> val = getParameter4f(str);
			float* valptr = &val.get()->x;
			memcpy(blockBuffer + offsets[i], valptr, 4 * sizeof(float));
		}
	}

	if (_uboId <= 0) {
		glGenBuffers(1, &_uboId);
	}
	
	glBindBuffer(GL_UNIFORM_BUFFER, _uboId);
	glBufferData(GL_UNIFORM_BUFFER, blockSize, blockBuffer, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_UNIFORM_BUFFER, _base, _uboId);

	return true;

}

void UniformBufferObject::apply(GLuint programId) {
	if (_dirty) {
		init(programId);
		_dirty = false;
	}
}

void UniformBufferObject::dirty() {
	_dirty = true;
}

UniformBufferObject::~UniformBufferObject()
{
}
